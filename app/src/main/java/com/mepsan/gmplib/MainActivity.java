package com.mepsan.gmplib;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mepsan.ecr.HuginPrinter;
import com.mepsan.ecr.TCPConnection;
import com.mepsan.match.DeviceInfo;
import com.mepsan.match.KeyContext;
import com.mepsan.match.MatchManager;

public class MainActivity extends AppCompatActivity{

    public static DeviceInfo Ex_Dev_Info;
    public static String TERMINAL_SERI_NO;
    private TCPConnection conn;
    private HuginPrinter huginPrinter;
    private KeyContext keyContext;
    private Button matchBtn;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getSharedPreferences(CommonUtil.PARAM_FILE, MODE_PRIVATE);
        conn = new TCPConnection(4444,"192.168.0.234");
        TERMINAL_SERI_NO = preferences.getString(CommonUtil.TERMINAL_NO,"TERM00000000");
        matchBtn = (Button) findViewById(R.id.btn_match);
        Button close = (Button) findViewById(R.id.btn_close);
        Button printHead = (Button) findViewById(R.id.btn_start);
        Button sign = (Button) findViewById(R.id.btn_sign);
        Button item1 = (Button) findViewById(R.id.btn_item);
        Button item2 = (Button) findViewById(R.id.btn_item_2);
        Button pay = (Button) findViewById(R.id.btn_pay);
        Button printZ = (Button) findViewById(R.id.btn_z);
        Button setDep = (Button) findViewById(R.id.btn_set_dp);
        Button getDep = (Button) findViewById(R.id.btn_get_dp);
        Button auth = (Button) findViewById(R.id.btn_auth);
        Button getCustom = (Button) findViewById(R.id.btn_get_custom);
        Button setCustom = (Button) findViewById(R.id.btn_set_custom);
        Button custom = (Button) findViewById(R.id.btn_cust);
        Button custom2 = (Button) findViewById(R.id.btn_cust_2);
        Button custom3 = (Button) findViewById(R.id.btn_cust_3);
        Button custom4 = (Button) findViewById(R.id.btn_cust_4);
        Button custom5 = (Button) findViewById(R.id.btn_cust_5);
        Button custom6 = (Button) findViewById(R.id.btn_cust_6);
        /* Not to match every time
        if (preferences.contains(CommonUtil.RND_EX_ECR)){
            String exEcrStr = preferences.getString(CommonUtil.RND_EX_ECR, "null");
            if (!exEcrStr.equals("null")){
                byte[] exAndEcr = DataConvert.hexStringToByteArray(exEcrStr);
                keyContext = new KeyContext(null);
                keyContext.setKeyIV(exAndEcr);
                huginPrinter = new HuginPrinter(0,keyContext);
                matchBtn.setText("Close Connection");
            }

        }*/

        auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("AUTH Cashier " + huginPrinter.signInCashier(9,"14710"));
                        return null;
                    }
                }.execute();
            }
        });
        getCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("Enter Service " + huginPrinter.enterServiceMode("12345"));
                        return null;
                    }
                }.execute();
            }
        });
        setCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        for (int i=0;i<10;i++)
                            System.out.println("Get PLU " + huginPrinter.getProduct(i));
                        return null;
                    }
                }.execute();
            }
        });
        custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("Save PRODUCT " + huginPrinter.setDepartment(4,"Eti",4,19,0));
                        System.out.println("Save PRODUCT " + huginPrinter.setDepartment(5, "KDV5", 5, 19, 0));
                        System.out.println("Save PRODUCT " + huginPrinter.saveProduct(4,"Eti",3,0.88,0,null,0));
                        System.out.println("Save PRODUCT " + huginPrinter.saveProduct(5, "Ülker", 4, 0.88, 0, null, 0));
                        return null;
                    }
                }.execute();
            }
        });
        custom2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                       /* for (int i=0;i<14;i++)
                            System.out.println("DEP "+ i+" -> "+ huginPrinter.getDepartment(i));*/
                        System.out.println("Cash Out " + huginPrinter.signInCashier(2,"123321"));
                        return null;
                    }
                }.execute();
            }
        });
        custom3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("Print EFT " + huginPrinter.exitServiceMode("12345"));
                        return null;
                    }
                }.execute();
            }
        });
        custom4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        for (int i=0;i<12;i++)
                            System.out.println("VAT RATES " + i + " -> " + huginPrinter.getDepartment(i));
                        return null;
                    }
                }.execute();
            }
        });

        custom5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("Cash Out " + huginPrinter.setVATRate(7, 7));
                        return null;
                    }
                }.execute();
            }
        });

        custom6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        for (int i=0;i<12;i++)
                            System.out.println("VAT RATES " + i + " -> " + huginPrinter.getVATRate(i));
                        return null;
                    }
                }.execute();
            }
        });

        setDep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("IPTAL " + huginPrinter.voidReceipt());
                        return null;
                    }
                }.execute();
            }
        });

        getDep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println(huginPrinter.checkPrinterStatus());
                        return null;
                    }
                }.execute();

            }
        });

        printZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("Z Result " + huginPrinter.printZReport());
                        return null;
                    }
                }.execute();

            }
        });

        matchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (matchBtn.getText().toString().equalsIgnoreCase("match")){
                    new Connect().execute();
                }else {
                    preferences.edit().remove(CommonUtil.RND_EX_ECR).apply();
                    keyContext = null;
                    conn.close();
                    matchBtn.setText("MATCH");

                }

            }
        });
        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        String[] pars = huginPrinter.signInCashier(6, "1357").split("\\|");
                        for (String par : pars)
                            System.out.println("Pars " + par);
                        return null;
                    }
                }.execute();
            }
        });

        item1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                       System.out.println("Print Item " + huginPrinter.printItem(1, 2, -1, null, 1, -1));
                       System.out.println("Print Item " + huginPrinter.printItem(3, 1, -1, null, 5, -1));
                       return null;
                    }
                }.execute();
            }
        });

        item2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncTask<Void,Void,Void>(){
                   @Override
                   protected Void doInBackground(Void... params) {
                       System.out.println("Print Item " + huginPrinter.printItem(2, 2, 6, "TadellI", 1, 0));
                       System.out.println("Print Item " + huginPrinter.printItem(2, 2, 6, "TadellI", 2, 0));
                       System.out.println("Print Item " + huginPrinter.printItem(2, 2, 6, "TadellI", 3, 0));
                       System.out.println("Print Item " + huginPrinter.printItem(2, 2, 6, "TadellI", 4, 0));
                       System.out.println("Print Item " + huginPrinter.printItem(2, 2, 6, "TadellI", 5, 0));
                       System.out.println("Print Item " + huginPrinter.printItem(2, 2, 6, "TadellI", 6, 0));
                       System.out.println("Print Item " + huginPrinter.printItem(2, 2, 6, "TadellI", 7, 0));
                       //System.out.println("Print Item " + huginPrinter.printItem(4, 1, -1, null, -1, -1));
                       //System.out.println("Print Item " + huginPrinter.printItem(5, 1, -1, null, -1, -1));
                       return null;
                   }
               }.execute();
            }
        });

        printHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new AsyncTask<Void,Void,Void>(){
                   @Override
                   protected Void doInBackground(Void... params) {
                       System.out.println("Print DOC HEADER " + huginPrinter.printDocumentHeader());
                       return null;
                   }
               }.execute();
            }
        });

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              new AsyncTask<Void,Void,Void>(){
                  @Override
                  protected Void doInBackground(Void... params) {
                      for (int i=0;i<5;i++)
                          System.out.println("Credit " + huginPrinter.getCreditInfo(i));
                      System.out.println("Print Payment " + huginPrinter.printPayment(1, 0, 50));
                      return null;
                  }
              }.execute();

            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... params) {
                        System.out.println("Print Payment " + huginPrinter.closeReceipt(false));
                        return null;
                    }
                }.execute();
            }
        });
    }

    private class Connect extends AsyncTask<Void,Void,Boolean>{

        private MatchManager mManager;
        public Connect() {
            String brand = "MEPSAN";
            String model = "A1088";
            String serial = "7789548";
            String version = "2.8";
            KeyContext.TERMINAL_NO = "FP00001849";
            DeviceInfo exDevice = new DeviceInfo(brand,model,serial,version);
            mManager = new MatchManager(0,exDevice,conn);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return mManager.match();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean){
                Toast.makeText(MainActivity.this,"Device Matched",Toast.LENGTH_SHORT).show();
                huginPrinter = new HuginPrinter(mManager.getKeyContext(),conn);
                matchBtn.setText("Close Connection");
                /*SharedPreferences.Editor editor = preferences.edit();
                editor.putString(CommonUtil.RND_EX_ECR,DataConvert.byteArrayToHexString(keyContext.getExAndEcr()));
                editor.apply();*/
            }else Toast.makeText(MainActivity.this,"Problem Happened",Toast.LENGTH_SHORT).show();
        }
    }

}
