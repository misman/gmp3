package com.mepsan.gmp;

import java.nio.ByteBuffer;

/**
 * Created by misman on 12.10.2015.
 */
public class GMPUtil {

    public static final int MATCH_INIT_REQ = 0xFF8A60;
    public static final int MATCH_INIT_RESP = 0xFF8E60;
    public static final int MATCH_KEY_EX_REQ = 0xFF8A62;
    public static final int MATCH_KEY_EX_RESP = 0xFF8E62;
    public static final int MATCH_CLOSE_REQ = 0xFF8A63;
    public static final int MATCH_CLOSE_RESP = 0xFF8E63;
    public static final int MATCH_ECHO_REQ = 0xFF8A64;
    public static final int MATCH_ECHO_RESP = 0xFF8E64;

    //GMP Constants
    public static final int LEN_SERIAL = 0x0C;
    public static final int LEN_EX_SERIAL = 0x10;
    public static final int LEN_SEQUENCE = 0x03;
    public static final int LEN_DATE = 0x03;
    public static final int LEN_TIME = 0x03;
    public static final int LEN_RESP_CODE = 0x02;
    public static final int LEN_DATA_TAG = 0x03;
    public static final int LEN_GRUP_TAG = 0x02;
    public static final int LEN_FISCAL_COMMAND = 0x01;
    public static final int LEN_LENGTH = 0x01;
    public static final int LEN_RANDOM_NUMBER = 0x10;
    public static final int LEN_VERSION = 0x20;

    //GMP Common Tags
    public static final int TAG_SEQUNCE = 0xDF8208;
    public static final int TAG_OP_DATE = 0xDF8209;
    public static final int TAG_OP_TIME = 0xDF820A;
    public static final int TAG_RESP_CODE = 0xDF820D;

    //GMP Grup Tags
    public static final int GROUP_HEADER = 0xDF02;
    public static final int GROUP_BLOCK = 0xDF40;
    public static final int GROUP_MATCH = 0xDF65;

    //GMP Data Tags
    public static final int DT_HOSTLOCALIP = 0xDFC105;
    public static final int DT_IP = 0xDFC106;
    public static final int DT_EX_BRAND = 0xDFC107;
    public static final int DT_EX_MODEL = 0xDFC108;
    public static final int DT_EX_SERIAL = 0xDFC109;

    public static final int DT_ECR_BRAND = 0xDFC10A;
    public static final int DT_ECR_MODEL = 0xDFC10B;
    public static final int DT_ECR_SERIAL = 0xDFC10C;
    public static final int DT_RANDOM_NUMBER = 0xDFC10D;
    public static final int DT_ECR_RANDOM_NUMBER = 0xDFC10E;

    public static final int DT_MOD_KEY = 0xDFC101;
    public static final int DT_EXP_KEY = 0xDFC102;
    public static final int DT_ENC_KEY = 0xDFC103;
    public static final int DT_CHK_VAL = 0xDFC104;

    public static final int DT_ECR_CERT = 0xDFEF01;
    public static final int DT_EXT_DH_PUB_KEY = 0xDFEF03;
    public static final int DT_ERROR_CODE = 0xDFEF06;
    public static final int DT_ECR_POS_INDEX = 0xDFEF09;
    public static final int DT_ECR_SIGNED_A = 0xDFEF0A;
    public static final int DT_DRIVER_VERSION = 0xDFEF0B;
    public static final int DT_DH_P = 0xDFEF0C;
    public static final int DT_DH_G = 0xDFEF0D;
    public static final int DT_DH_CHECK_HASH = 0xDFEF0E;

    public static final byte ACK = 0x06;
    public static final byte NACK = 0x15;
    public static final byte STX = 0x02;
    public static final byte ETX = 0x03;

    public static GMPMessage parseData(byte[] buffer){
        ByteBuffer bbf = ByteBuffer.wrap(buffer);
        byte[] terminalBytes = new byte[12];
        byte[] messageTag = new byte[3];
        bbf.get(terminalBytes);
        System.out.println("Terminal No -> " + new String(terminalBytes));
        bbf.get(messageTag);

        int msgLen = GMPUtil.getLength(bbf);
        /*String messTag = "";
        for (byte by : messageTag) {
            messTag += String.format("%02x", by & 0xff);
        }
        System.out.println("Message Tag " + messTag);
        System.out.println("Message Len " + msgLen);*/
        int end = bbf.position() + msgLen;
        byte[] tagBytes = new byte[GMPUtil.LEN_DATA_TAG];
        GMPMessage message = new GMPMessage(DataConvert.byte3ToInt(messageTag));
        while (bbf.position() != end) {
            bbf.get(tagBytes);
            int tag = DataConvert.byte3ToInt(tagBytes);
            int length = GMPUtil.getLength(bbf);
            byte[] value = new byte[length];
            bbf.get(value);
            GMPField field = new GMPField(tag, value);
            message.addItem(field);
        }
        return message;
    }

    public static byte[] getByteLength(int value){
        byte[] length;
        if(value > 0xFF) {
            length = new byte[3];
            length[0] = (byte) 0x82;
            length[1] = (byte) (value >>> 8);
            length[2] = (byte) value;

        }else if (value > 0x7F){
            length = new byte[2];
            length[0] = (byte) 0x81;
            length[1] = (byte) value;
        }else length = new byte[]{(byte)value};

        return length;
    }

    public static int getLength(ByteBuffer buffer){
        int len;
        byte first = buffer.get();
        switch (first)
        {
            case (byte)0x81:
                len = buffer.get() & 0xFF;
                break;
            case (byte)0x82:
                len = ((buffer.get() & 0xff) << 8) | (buffer.get() & 0xff);
                break;
            default:
                len = first & 0xFF;
                break;
        }
        return len;
    }
}
