package com.mepsan.gmp;

/**
 * Created by misman on 12.10.2015.
 */
public interface GMPItem {
    int getTag();
    byte[] getLength();
    byte[] getValue();
}
