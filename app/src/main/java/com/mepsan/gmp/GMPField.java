package com.mepsan.gmp;

import java.io.UnsupportedEncodingException;

/**
 * Created by misman on 12.10.2015.
 */
public class GMPField implements GMPItem {

    private int tag;
    private byte[] length;
    private byte[] value;

    public GMPField(int tag, byte[] value) {
        this.tag = tag;
        this.value = value;
        this.length = GMPUtil.getByteLength(value.length);
    }


    public GMPField(int tag, String str) {
        this.tag = tag;
        try {
            this.value = str.getBytes("windows-1254");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.length = GMPUtil.getByteLength(value.length);
    }

    @Override
    public int getTag() {
        return tag;
    }

    @Override
    public byte[] getLength() {
        return length;
    }

    @Override
    public byte[] getValue() {
        return value;
    }
}
