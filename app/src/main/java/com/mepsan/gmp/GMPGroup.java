package com.mepsan.gmp;

import java.util.ArrayList;

/**
 * Created by misman on 12.10.2015.
 */
public class GMPGroup implements GMPItem {

    private int tag;
    private ArrayList<GMPField> fields;

    public GMPGroup(int tag) {
        this.tag = tag;
        fields = new ArrayList<>();
    }

    @Override
    public int getTag() {
        return tag;
    }

    @Override
    public byte[] getLength() {
        int length = 0;
        for (GMPField field : fields){
            length+= GMPUtil.LEN_DATA_TAG + GMPUtil.LEN_LENGTH
                    + field.getLength().length;
        }
        return fields.get(0).getLength();
    }

    @Override
    public byte[] getValue() {
        /*ByteBuffer byteBuffer = ByteBuffer.allocate(getLength());
        for (GMPField field : fields){
            byte[] tag = DataConvert.intToByteArray(field.getTag());
            byteBuffer.put(tag);
            byteBuffer.put((byte) field.getLength());
            byteBuffer.put(field.getValue());
        }
        return byteBuffer.array();*/
        return new byte[]{};
    }

    public ArrayList<GMPField> getFields() {
        return fields;
    }

    public void addField(GMPField field){
        fields.add(field);
    }

    public GMPField findField(int tag){
        for (GMPField field : fields){
            if (tag == field.getTag()){
                return field;
            }
        }

        return null;
    }
}
