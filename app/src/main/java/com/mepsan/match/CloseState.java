package com.mepsan.match;

import com.mepsan.gmp.DataConvert;
import com.mepsan.gmp.GMPField;
import com.mepsan.gmp.GMPMessage;
import com.mepsan.gmp.GMPUtil;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by misman on 05.11.2015.
 */
public class CloseState extends SyncState {

    public CloseState(KeyContext kContext, ProcessInfo rqIn) {
        super(kContext, rqIn);
    }

    @Override
    public byte[] getMessage() {

        byte[] checkData = new byte[64];
        Arrays.fill(checkData, 0, 32, (byte) 0x20);
        Arrays.fill(checkData, 32, 64, (byte) 0xFF);

        GMPMessage message = new GMPMessage();
        MatchManager.addProInfo(message, getRqInfo());
        MatchManager.addExInfo(message, getKeyContext().getExInfo());
        MatchManager.addEcrInfo(message, getKeyContext().getEcrInfo());
        byte[] encCheck = getKeyContext().encData(checkData);
        message.addItem(new GMPField(GMPUtil.DT_DH_CHECK_HASH,encCheck));
        message.addItem(new GMPField(GMPUtil.DT_ECR_POS_INDEX,
                DataConvert.hexStringToByteArray(getKeyContext().getIndexEx())));
        byte[] pack = MatchManager.addMessageTag(message,GMPUtil.MATCH_CLOSE_REQ);
        setPack(pack);
        return pack;
    }

    @Override
    public void decompose(GMPMessage message) {
        GMPField customField;
        customField = message.findField(GMPUtil.TAG_SEQUNCE);
        String proIdStr = DataConvert.byteArrayToHexString(customField.getValue());

        customField = message.findField(GMPUtil.TAG_OP_DATE);
        byte[] date = customField.getValue();

        customField = message.findField(GMPUtil.TAG_OP_TIME);
        byte[] time = customField.getValue();

        Date dateTime = DataConvert.byteToDateTime(date, time);
        setRsInfo(new ProcessInfo(dateTime,proIdStr));

        customField = message.findField(GMPUtil.DT_ERROR_CODE);
        String errorCode = DataConvert.byteArrayToHexString(customField.getValue());
        setErrorCode(errorCode);

        customField = message.findField(GMPUtil.DT_ECR_POS_INDEX);
        String exIndex = DataConvert.byteArrayToHexString(customField.getValue());
        getKeyContext().generateKeyIV();
    }
}
