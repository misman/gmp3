package com.mepsan.match;

import java.io.ByteArrayInputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by misman on 02.11.2015.
 */
public class CertValidation {
    public static final String[] certErrors = new String[]{"Sertifika Formatı veya Tipi Uygun Değil", "Sertifika Yayıncısı Bilinmiyor",
            "Sertifika Amacına Uygun Kullanılmıyor Key Usage Alanı Kontrol Edilmeli","Sertifika Henüz Kullanıma Açık Değil",
            "Sertifika Kullanım Süresi Dolmuş"};
    public static final String[] certProviders = new String[]{"Odeme Kaydedici Cihaz Elektronik Sertifika Hizmet Saglayicisi-S1"};
    private byte[] certBytes;
    private String certError;
    private X509Certificate certificate;

    public CertValidation(byte[] certBytes) {
        this.certBytes = certBytes;
    }

    public boolean startValidation() {
        CertificateFactory cf;
        try {
             cf = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e) {
            e.printStackTrace();
            return false;
        }
        try {
            certificate = (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(certBytes));
        } catch (CertificateException e) {
            e.printStackTrace();
            setCertError(0);
            return false;
        }
        if (!checkProvider()){
            setCertError(1);
            return false;
        }

        boolean[] usages = certificate.getKeyUsage();
        boolean usage = usages[0] && usages[2];
        if (!usage){
            setCertError(2);
        }

        try {
            certificate.checkValidity();
        } catch (CertificateNotYetValidException e) {
            e.printStackTrace();
            setCertError(3);
            return false;
        } catch (CertificateExpiredException e) {
            e.printStackTrace();
            setCertError(4);
            return false;
        }

       /* try {
            RSAPublicKey publicrsa = (RSAPublicKey) certificate.getPublicKey();
            Signature signature = Signature.getInstance("SHA256WithRSA");
            signature.initVerify(publicrsa);
            if(signature.verify(certificate.getTBSCertificate())){
                System.out.println("Onaylandı");
            }else System.out.println("OnaylanMAdı");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }*/

        String subjectToStr = certificate.getSubjectDN().toString();
        String[] subjects = subjectToStr.split(",");
        String terminalNo = subjects[0].substring(subjects[0].indexOf("=")+1);
        System.out.println("Terminal No is " + terminalNo);
        return true;
    }


    public boolean startValid(){
        CertificateFactory cf;
        try {
            cf = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e) {
            e.printStackTrace();
            return false;
        }
        try {
          Collection certifCollect = cf.generateCertificates(new ByteArrayInputStream(certBytes));
            System.out.println("Size " + certifCollect.size());
            Iterator i = certifCollect.iterator();
            while (i.hasNext()) {
                Certificate cert = (Certificate)i.next();
            }


        } catch (CertificateException e) {
            e.printStackTrace();
            setCertError(0);
            return false;
        }
       /* if (!checkProvider()){
            setCertError(1);
            return false;
        }

        try {
            CertPathBuilder certPathBuilder = CertPathBuilder.getInstance("PKIX");
            X509CertSelector certSelector = new X509CertSelector();
            certSelector.setCertificate(certificate);


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }*/

        return true;


    }

    private boolean checkProvider(){
        String dn = certificate.getIssuerDN().getName();
        String[] values = dn.split(",");
        for (String cn : values){
            if(cn.startsWith("CN=")){
                cn = cn.substring(3);
                for (String provider : certProviders){
                    if (provider.equalsIgnoreCase(cn))
                        return true;
                }
            }
        }
        return false;
    }

    public String getCertError() {
        return certError;
    }

    private void setCertError(int errorNo) {
        certError = certErrors[errorNo];
    }

    public boolean checkSignedA(byte[] signByte, byte[] publicBytes) {
        try {
            Signature signature = Signature.getInstance("SHA256WithRSA");
            signature.initVerify(certificate.getPublicKey());
            signature.update(publicBytes);
            return signature.verify(signByte);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        }
        return false;
    }
}
