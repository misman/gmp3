package com.mepsan.match;

import com.mepsan.gmp.DataConvert;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.DHParameterSpec;
import javax.crypto.spec.DHPrivateKeySpec;
import javax.crypto.spec.DHPublicKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by misman on 03.11.2015.
 */
public class KeyContext {

    private static final String PRM_GMP3_PRF_LABEL = "GMP-3 istek";
    private static final String COMPUTE_KEYS_LABEL = "GMP-3 anahtarlar";
    private static final String CER_KAMU_SM_PRODUCER = "KAMU SM";

    private DeviceInfo exInfo, ecrInfo;
    private byte[] exRandom;
    private byte[] ecrRandom;
    private byte[] exAndEcr;
    private byte[] keyHMAC;
    private byte[] keyIV;
    private byte[] keyENC;
    private BigInteger dhP;
    private BigInteger dhG;
    private BigInteger dhB;
    private BigInteger dhA;
    private BigInteger dhMyPrivate;
    private String indexEx;
    private CertValidation certValidation;
    private Cipher encrypt;
    private Cipher decrypt;

    private static int AES_KEY_LENGTH = 32;
    public static String TERMINAL_NO;

    public KeyContext(DeviceInfo exDevice) {
        this.exInfo = exDevice;
    }

    public void setExRandom(byte[] exRandom) {
        this.exRandom = exRandom;
    }

    public void setDhA(BigInteger dhA) {
        this.dhA = dhA;
        generateOtherKeys();
        generateCiphers();
    }

    public byte[] encData(byte[] data){
        try {
            return encrypt.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    private void generateCiphers() {
        SecretKeySpec secret = new SecretKeySpec(keyENC, "AES");
        IvParameterSpec IVParam = new IvParameterSpec(keyIV);

        try {
            encrypt = Cipher.getInstance("AES/CBC/NoPadding");
            encrypt.init(Cipher.ENCRYPT_MODE, secret, IVParam);

            decrypt = Cipher.getInstance("AES/CBC/NoPadding");
            decrypt.init(Cipher.DECRYPT_MODE, secret, IVParam);
        } catch (NoSuchAlgorithmException | InvalidKeyException |
                InvalidAlgorithmParameterException | NoSuchPaddingException e) {
            e.printStackTrace();
        }

    }

    private void generateOtherKeys() {
        BigInteger dhPreMaster = dhA.modPow(dhMyPrivate,dhP);
        byte[] dhPre = DataConvert.bigToBA(dhPreMaster) ;
        byte[] masterSecret = tlsKey(dhPre, PRM_GMP3_PRF_LABEL);
        keyHMAC = tlsKey(masterSecret, COMPUTE_KEYS_LABEL);
        keyIV = tlsKey(keyHMAC, COMPUTE_KEYS_LABEL);
        keyIV = Arrays.copyOf(keyIV, 16);
        keyENC = tlsKey(keyIV,COMPUTE_KEYS_LABEL);
    }

    public byte[] encryptMessage(byte[] data,byte[] hash){
        int totalLength = hash.length + data.length;

        //Padding Operation
        int padLength = AES_KEY_LENGTH - (totalLength % AES_KEY_LENGTH);
        if (padLength==0)
            padLength = AES_KEY_LENGTH;

        byte[] paddArray = new byte[padLength];
        Arrays.fill(paddArray, (byte) padLength);

        //Add Data and Mac
        byte[] message = ByteBuffer.allocate(totalLength+padLength)
                .put(paddArray).put(data).put(hash).array();

        return encData(message);
    }

    public void generateKeyIV(){
        keyIV = new byte[16];

        int i=0;
        for (byte b : exRandom)
            keyIV[i] = (byte) (b ^ ecrRandom[i++]);

        generateCiphers();
    }

    private byte[] tlsKey(byte[] key, String label) {
        byte[] labelBytes = label.getBytes();
        byte[] seed = ByteBuffer.allocate(exAndEcr.length + labelBytes.length)
                .put(labelBytes).put(exAndEcr).array();

        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key,"HmacSHA256");

            sha256_HMAC.init(secret_key);
            byte[] hasedSeed = sha256_HMAC.doFinal(seed);

            sha256_HMAC.reset();
            sha256_HMAC.init(secret_key);
            byte[] newHC = ByteBuffer.allocate(hasedSeed.length+seed.length)
                    .put(hasedSeed).put(seed).array();

            return sha256_HMAC.doFinal(newHC);
            /*ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[][] hmacs = new byte[3][];
            hmacs[0] = seed;
            for (int i=1;i<3;i++) {
                sha256_HMAC.reset();
                sha256_HMAC.init(secret_key);
                hmacs[i] = sha256_HMAC.doFinal(hmacs[i-1]);

                sha256_HMAC.reset();
                sha256_HMAC.init(secret_key);
                byte[] newHC = ByteBuffer.allocate(hmacs[i].length + seed.length)
                        .put(hmacs[i]).put(seed).array();
                baos.write(sha256_HMAC.doFinal(newHC));
            }
            byte[] result = baos.toByteArray();
            baos.close();
            return Arrays.copyOf(result,32);*/
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }


    public void setParams(BigInteger p, BigInteger g, String exIndex, byte[] randomEcr) {
        this.dhP = p;
        this.dhG = g;
        this.indexEx = exIndex;
        this.ecrRandom = randomEcr;
        this.exAndEcr = ByteBuffer.allocate(32).put(exRandom).put(ecrRandom).array();
    }

    public void dhGeneratePublic(){
        try {
            DHParameterSpec parameterSpec = new DHParameterSpec(dhP,dhG);
            KeyPairGenerator gen = KeyPairGenerator.getInstance("DH");
            gen.initialize(parameterSpec);

            KeyPair keyPair = gen.generateKeyPair();
            KeyFactory keyFactory = KeyFactory.getInstance("DH");
            DHPublicKeySpec dhPublicKeySpec = keyFactory.getKeySpec(keyPair.getPublic(), DHPublicKeySpec.class);
            DHPrivateKeySpec dhPrivateKeySpec = keyFactory.getKeySpec(keyPair.getPrivate(), DHPrivateKeySpec.class);
            dhB = dhPublicKeySpec.getY();
            dhMyPrivate = dhPrivateKeySpec.getX();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    public boolean isCertValid(byte[] certBytes){
        certValidation = new CertValidation(certBytes);
        return certValidation.startValidation();
    }

    public void setEcrInfo(DeviceInfo ecrInfo) {
        this.ecrInfo = ecrInfo;
    }

    public DeviceInfo getExInfo() {
        return exInfo;
    }

    public DeviceInfo getEcrInfo() {
        return ecrInfo;
    }

    public byte[] getDhB() {
        return DataConvert.bigToBA(dhB);
    }

    public String getIndexEx() {
        return indexEx;
    }

    public String getCertError() {
        return certValidation.getCertError();
    }

    public boolean checkSignedA(byte[] signedData) {
        return certValidation.checkSignedA(signedData, getDhA());
    }

    public byte[] getDhA() {
        return DataConvert.bigToBA(dhA);
    }

    public byte[] decData(byte[] encCheck) {
        try {
           return decrypt.doFinal(encCheck);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] getKeyHMAC() {
        return keyHMAC;
    }

    public byte[] getKeyENC() {
        return keyENC;
    }

    public byte[] getExAndEcr() {
        return exAndEcr;
    }

    public void genKeysMatched(byte[] exAndEcr, byte[] keyENC) {
        this.keyENC = keyENC;
        this.exAndEcr = exAndEcr;
        this.exRandom = new byte[16];
        this.ecrRandom = new byte[16];

        ByteBuffer bbf = ByteBuffer.wrap(exAndEcr);
        bbf.get(exRandom);
        bbf.get(ecrRandom);
        generateKeyIV();
    }
}
