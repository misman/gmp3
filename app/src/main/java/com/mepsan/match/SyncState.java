package com.mepsan.match;

import com.mepsan.gmp.GMPMessage;

/**
 * Created by misman on 03.11.2015.
 */
public abstract class SyncState {

    private ProcessInfo rqInfo, rsInfo;
    private KeyContext keyContext;
    private byte[] pack;
    private String errorCode;

    public SyncState(KeyContext kContext, ProcessInfo rqIn) {
        keyContext = kContext;
        rqInfo = rqIn;
    }

    public void setRsInfo(ProcessInfo rsInfo) {
        this.rsInfo = rsInfo;
    }

    public KeyContext getKeyContext() {
        return keyContext;
    }

    public ProcessInfo getRqInfo() {
        return rqInfo;
    }

    public ProcessInfo getRsInfo() {
        return rsInfo;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public byte[] getPack() {
        return pack;
    }

    public void setPack(byte[] pack) {
        this.pack = pack;
    }

    public abstract byte[] getMessage();
    public abstract void decompose(GMPMessage message);

}
