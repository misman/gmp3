package com.mepsan.match;

import com.mepsan.gmp.DataConvert;
import com.mepsan.gmp.GMPField;
import com.mepsan.gmp.GMPMessage;
import com.mepsan.gmp.GMPUtil;

import java.math.BigInteger;
import java.util.Date;

/**
 * Created by misman on 03.11.2015.
 */
public class KeyReqState extends SyncState {


    public KeyReqState(KeyContext keyContext,ProcessInfo rqIn) {
        super(keyContext,rqIn);
    }

    @Override
    public byte[] getMessage() {
        GMPMessage message = new GMPMessage();
        MatchManager.addProInfo(message, getRqInfo());
        MatchManager.addExInfo(message, getKeyContext().getExInfo());
        MatchManager.addEcrInfo(message, getKeyContext().getEcrInfo());
        message.addItem(new GMPField(GMPUtil.DT_EXT_DH_PUB_KEY, getKeyContext().getDhB()));
        message.addItem(new GMPField(GMPUtil.DT_ECR_POS_INDEX,
                DataConvert.hexStringToByteArray(getKeyContext().getIndexEx())));
        byte[] pack = MatchManager.addMessageTag(message, GMPUtil.MATCH_KEY_EX_REQ);
        setPack(pack);
        return pack;
    }

    @Override
    public void decompose(GMPMessage message) {
        GMPField customField;
        customField = message.findField(GMPUtil.TAG_SEQUNCE);
        String proIdStr = DataConvert.byteArrayToHexString(customField.getValue());

        customField = message.findField(GMPUtil.TAG_OP_DATE);
        byte[] date = customField.getValue();

        customField = message.findField(GMPUtil.TAG_OP_TIME);
        byte[] time = customField.getValue();

        Date dateTime = DataConvert.byteToDateTime(date, time);
        setRsInfo(new ProcessInfo(dateTime, proIdStr));

        customField = message.findField(GMPUtil.DT_EXT_DH_PUB_KEY);
        String dhAStr = DataConvert.byteArrayToHexString(customField.getValue());
        getKeyContext().setDhA(new BigInteger(dhAStr,16));

        customField = message.findField(GMPUtil.DT_ECR_SIGNED_A);
        boolean isOk = getKeyContext().checkSignedA(customField.getValue());
        System.out.println("Is OK " + isOk);

        if (isOk) {
            customField = message.findField(GMPUtil.DT_ERROR_CODE);
            String errorCode = DataConvert.byteArrayToHexString(customField.getValue());
            setErrorCode(errorCode);

            customField = message.findField(GMPUtil.DT_ECR_POS_INDEX);
            String exIndex = DataConvert.byteArrayToHexString(customField.getValue());
        }else {
            try {
                setErrorCode("Signature Verification Failed");
                throw new Exception("Signature Verification Error");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
