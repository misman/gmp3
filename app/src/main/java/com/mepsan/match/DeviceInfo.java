package com.mepsan.match;

/**
 * Created by misman on 20.10.2015.
 */
public class DeviceInfo {

    private String model;
    private String brand;
    private String version;
    private String serial;

    public DeviceInfo(String brand,String model, String serial,String version) {
        this.model = model;
        this.brand = brand;
        this.serial = serial;
        this.version = version;
    }

    public DeviceInfo(String brand, String model, String serial) {
        this.brand = brand;
        this.model = model;
        this.serial = serial;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getVersion() {
        if (version==null)
            return "-1";
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

}
