package com.mepsan.ecr;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;

import com.mepsan.serial.driver.CdcAcmSerialDriver;
import com.mepsan.serial.driver.ProbeTable;
import com.mepsan.serial.driver.UsbSerialDriver;
import com.mepsan.serial.driver.UsbSerialPort;
import com.mepsan.serial.driver.UsbSerialProber;
import com.mepsan.serial.util.SerialInputOutputManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by misman on 14.10.2015.
 */
public class SerialConnection implements Connection{

    private Activity context;
    private UsbSerialPort serialPort;
    private UsbManager manager;
    private UsbDeviceConnection connection;
    private boolean isSerialConnected;
    private int baundRate;
    private SerialInputOutputManager serialInOutMan;

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static final int VENDOR_ID=12;
    private static final int PRODUCT_ID=10;

    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    private SerialInputOutputManager.Listener serialListener = new SerialInputOutputManager.Listener() {
        @Override
        public void onNewData(byte[] data) {

        }

        @Override
        public void onRunError(Exception e) {

        }
    };

    private BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(ACTION_USB_PERMISSION)){
                synchronized (this){
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if(device != null){
                           System.out.println("Permission Granted For Serial");
                        }
                    }
                }
            }
        }
    };

    public SerialConnection(Activity context,int baundRate) {
        this.context = context;
        this.baundRate = baundRate;
        requestPermissionForDevice();
    }

    @Override
    public void open() {
        isSerialConnected = initializeSerial();
        startIoManager();
    }

    private void requestPermissionForDevice() {
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        context.registerReceiver(mUsbReceiver, filter);
        manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        PendingIntent mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
        HashMap<String,UsbDevice> deviceList = manager.getDeviceList();
        Iterator<UsbDevice> iterator = deviceList.values().iterator();

        while (iterator.hasNext()){
            UsbDevice usbDevice = iterator.next();
            String deviceName = usbDevice.getDeviceName();
            int productId = usbDevice.getProductId();
            int vendorId = usbDevice.getVendorId();

            if(vendorId==VENDOR_ID && productId==PRODUCT_ID){
                UsbDevice device = deviceList.get(deviceName);
                if (manager.hasPermission(device)) {
                    System.out.println("Permission Granted For Serial");
                }else {
                    manager.requestPermission(device,mPermissionIntent);
                }
            }
        }

    }

    private boolean initializeSerial(){
        ProbeTable customTable = new ProbeTable();
        customTable.addProduct(0x04D8, 0x000A, CdcAcmSerialDriver.class);
        UsbSerialProber prober = new UsbSerialProber(customTable);

        List<UsbSerialDriver> driverList = prober.findAllDrivers(manager);
        if(driverList.isEmpty()){
            System.out.println("DriverList is Empty");
            return false;
        }
        UsbSerialDriver driver = driverList.get(0);
        System.out.println(" INFO DRIVER " + driver.getDevice().toString());
        connection = manager.openDevice(driver.getDevice());

        if(connection==null){
            System.out.println("Connection not available");
            return false;
        }

        System.out.println("Port count is " + driver.getPorts().size());
        serialPort = driver.getPorts().get(0);
        try {
            serialPort.open(connection);
            serialPort.setParameters(baundRate, UsbSerialPort.DATABITS_7,
                    UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_EVEN);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void stopIoManager() {
        if (serialInOutMan != null) {
            serialInOutMan.stop();
            serialInOutMan = null;
        }
    }

    private void startIoManager() {
        if (serialPort != null) {
           serialInOutMan = new SerialInputOutputManager(serialPort, serialListener);
           mExecutor.submit(serialInOutMan);
        }
    }

    @Override
    public boolean isOpen() {
        return isSerialConnected;
    }

    @Override
    public void close() {
        stopIoManager();
        connection.close();
        isSerialConnected = false;
    }

    @Override
    public byte[] read() {
        return new byte[0];
    }

    @Override
    public boolean sendData(byte[] data) {
        return false;
    }

    @Override
    public Object toObject() {
        return null;
    }

    @Override
    public void setReadTimeOut(int timeOut) {
        // Time Out ???
    }
}
