package com.mepsan.ecr;

/**
 * Created by misman on 14.10.2015.
 */
public interface Connection {
    void open();
    boolean isOpen();
    void close();
    byte[] read();
    boolean sendData(byte[] data);
    Object toObject();
    void setReadTimeOut(int timeOut);
}
