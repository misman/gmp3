package com.mepsan.ecr;

/**
 * Created by misman on 20.11.2015.
 */
public class GeneralResponse {

    private static String[] ErrorMessages = {
        "WRITE OR CONNECTION PROBLEM",
        "READ PROBLEM", "ANSWER PARSE PROBLEM"
    };

    private String errorMessage;
    private HuginResponse hResponse;

    public GeneralResponse() {
    }

    public void setErrorMessage(int errorId) {
        this.errorMessage = ErrorMessages[errorId];
    }

    public void sethResponse(HuginResponse hResponse) {
        this.hResponse = hResponse;
    }

    public HuginResponse gethResponse() {
        return hResponse;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
