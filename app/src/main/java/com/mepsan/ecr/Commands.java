package com.mepsan.ecr;

/**
 * Created by misman on 12.11.2015.
 */
public class Commands {
    public static final byte NULL = 0;
    public static final byte LOGO = 0x11;
    public static final byte VAT = 0x12;
    public static final byte DEPARTMENT = 0x13;
    public static final byte PLU = 0x14;
    public static final byte CREDIT = 0x15;
    public static final byte MAIN_CAT = 0x16;
    public static final byte SUB_CAT = 0x17;
    public static final byte CASHIER = 0x18;
    public static final byte PRG_OPTIONS = 0x19;
    public static final byte FCURRENCY = 0x1A;
    public static final byte TSM_IP_PORT = 0x1B;
    public static final byte GRAPHIC_LOGO = 0x1C;
    public static final byte NETWORK = 0x1D;

    public static final byte START_RCPT = 0x21;
    public static final byte SALE_ITEM = 0x22;
    public static final byte ADJUSTMENT = 0x23;
    public static final byte CORRECTION = 0x24;
    public static final byte VOID_ITEM = 0x25;
    public static final byte SUBTOTAL = 0x26;
    public static final byte PAYMENT = 0x27;
    public static final byte CLOSE_RCPT = 0x28;
    public static final byte VOID_RCPT = 0x29;
    public static final byte CUSTOMER_NOTE = 0x2A;
    public static final byte RCPT_BARCODE = 0x2B;

    public static final byte X_DAILY = 0x31;
    public static final byte X_PLU = 0x32;
    public static final byte X_DETAIL_SALE = 0x33;

    public static final byte Z_DAILY = 0x41;
    public static final byte Z_FM_ZZ = 0x42;
    public static final byte Z_FM_ZZ_DETAIL = 0x43;
    public static final byte Z_FM_DATE = 0x44;
    public static final byte Z_FM_DATE_DETAIL = 0x45;
    public static final byte Z_ENDDAY = 0x46; //Endday bank
    public static final byte CMD_Z_OLD_DAILY = 0x47;

    public static final byte EJ_DETAIL = 0x51;
    public static final byte EJ_RCPTCOPY_ZR = 0x52;//both single and periodic
    public static final byte EJ_RCPTCOPY_DATE = 0x53;//both single and periodic. either daily

    public static final byte SRV_CLEAR_DAILY = 0x61;
    public static final byte SRV_SET_DATETIME = 0x62;
    public static final byte SRV_FACTORY_SETTINGS = 0x63;
    public static final byte SRV_STOP_FM = 0x64;
    public static final byte SRV_EXTERNAL_DEV_SETTINGS = 0x65;
    public static final byte SRV_UPDATE_FIRMWARE = 0x66;
    public static final byte SRV_PRINT_LOGS = 0x67;
    public static final byte SRV_CREATE_DB = 0x68;
    public static final byte SRV_EXIT_SERVICE = 0x6F;

    public static final byte INFO_LAST_Z = 0x71;
    public static final byte INFO_LAST_RCPT = 0x72;
    public static final byte INFO_DRAWER = 0x73;

    public static final byte STATUS = (byte) 0x81;
    public static final byte LAST_RESPONSE = (byte) 0x82;
    public static final byte BREAK = (byte) 0x83;
    public static final byte START_FM = (byte) 0x84;
    public static final byte FISCALIZE = (byte) 0x85;
    public static final byte INIT_EJ = (byte) 0x86;
    public static final byte CASHIER_LOGIN = (byte) 0x87;
    public static final byte CASHIER_LOGOUT = (byte) 0x88;
    public static final byte CASH_IN = (byte) 0x89;
    public static final byte CASH_OUT = (byte) 0x8A;
    public static final byte START_NF_RCPT = (byte) 0x8B;
    public static final byte ADD_NF_LINE = (byte) 0x8C;
    public static final byte END_NF_RCPT = (byte) 0x8D;
    public static final byte EJ_LIMIT = (byte) 0x8E;
    public static final byte START_SERVICE = (byte) 0x8F;
    public static final byte ENTER_SERVICE = (byte) 0x90;
    public static final byte FILE_TRANSFER = (byte) 0x91;
    public static final byte OPEN_DRAWER = (byte) 0x92;
    public static final byte EFT_PAYMENT = (byte) 0xA0;
}
