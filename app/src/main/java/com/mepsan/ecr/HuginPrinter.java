package com.mepsan.ecr;

import com.mepsan.gmp.CRC16;
import com.mepsan.gmp.DataConvert;
import com.mepsan.gmp.GMPField;
import com.mepsan.gmp.GMPGroup;
import com.mepsan.gmp.GMPItem;
import com.mepsan.gmp.GMPMessage;
import com.mepsan.gmp.GMPUtil;
import com.mepsan.match.KeyContext;
import com.mepsan.match.ProcessInfo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by misman on 12.11.2015.
 */
public class HuginPrinter {

    public class Settings
    {
       public static final int CUTTER = 0;
       public static final int PAY_WITH_EFT = 1;
       public static final int RECEIPT_LIMIT = 2;
       public static final int GRAPHIC_LOGO = 3;
       public static final int RECEIPT_BARCODE = 4;
    }

    public class AdjustType
    {
        public static final byte Fee = 0;
        public static final byte PercentFee = 1;
        public static final byte Discount = 2;
        public static final byte PercentDiscount = 3;
    }
    public class PaymentType
    {
        public static final int CASH = 0;
        public static final int CREDIT = 1;
        public static final int CHECK = 2;
        public static final int FCURRENCY = 3;
    }

    private static final int REQUEST_MSG_ID = 0xFF8B21;
    private static final int RESPONSE_MSG_ID = 0xFF8F21;
    private KeyContext keyContext;
    private int processId=0;
    private DecimalFormat df = new DecimalFormat("000.000");
    private Connection conn;

    public HuginPrinter(int proId, KeyContext keyContext, Connection connection) {
        this.keyContext = keyContext;
        this.processId = proId;
        this.conn = connection;
        if (processId>999999)
            processId=0;
    }

    public HuginPrinter(KeyContext keyContext, Connection connection) {
        this.keyContext = keyContext;
        this.conn = connection;
    }

    public ProcessInfo getProcessInfo(){
        if(processId>999999){
            processId=0;
        }

        String procesStr = String.format("%06d", ++processId);
        return new ProcessInfo(procesStr);
    }

    public void addProInfo(GMPMessage message, ProcessInfo procInfo){
        message.addItem(new GMPField(GMPUtil.TAG_SEQUNCE, procInfo.getProByte()));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(procInfo.getDate())));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(procInfo.getDate())));

    }

    public void addProWithBaos(ByteArrayOutputStream baos, ProcessInfo processInfo, byte command){
        try {
            baos.write(DataConvert.intToByte3(GMPUtil.TAG_SEQUNCE));
            baos.write((byte)3);
            baos.write(processInfo.getProByte());
            baos.write(DataConvert.dateTimeToBCD(processInfo.getDate()));
            baos.write(DataConvert.intToByte3(ECRUtil.FPU_FISCAL_COMMAND));
            baos.write((byte)1);
            baos.write(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String doubleToStr(byte[] bcdDouble) {
        String value = DataConvert.byteArrayToHexString(bcdDouble);
        value = value.substring(0, 7) + "." + value.substring(7, value.length());
        double dVal;
        try {
            dVal = df.parse(value).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
            dVal = -1;
        }
        return String.valueOf(dVal);
    }

    private GeneralResponse sendAndRead(byte[] data){
        GeneralResponse gR = new GeneralResponse();
        if(conn.sendData(data)){
            byte[] result = conn.read();
            if(result.length>0){
                HuginResponse hR = parse(result);
                if (hR != null){
                    gR.sethResponse(hR);
                }else {
                    gR.setErrorMessage(2);
                }
            }
            else gR.setErrorMessage(1);
        }else {
            gR.setErrorMessage(0);
        }
        return gR;
    }

    //region Program Functions

    public String setDepartment(int id, String name, int vatId, double price, int weighable) {
        String idStr = String.format("%02d", id);
        String vatIdStr = String.format("%02d", vatId);
        String priceStr = df.format(price).replace(".", "");
        String weighStr = String.format("%02d", weighable);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.DEPARTMENT}));
        message.addItem(new GMPField(ECRUtil.DEPT, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.NOTE, name));
        message.addItem(new GMPField(ECRUtil.VATGROUP_NO, DataConvert.hexStringToByteArray(vatIdStr)));
        message.addItem(new GMPField(ECRUtil.AMOUNT, DataConvert.hexStringToByteArray(priceStr)));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, DataConvert.hexStringToByteArray(weighStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                GMPGroup detail = hResponse.getDetail();
                if (detail!=null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }else rsDetail.add("null");


                    detailItem = detail.findField(ECRUtil.VATGROUP_NO);
                    if (detailItem != null) {
                        String vatG = DataConvert.byteArrayToHexString(detailItem.getValue());
                        int vatGroup = Integer.parseInt(vatG);
                        vatGroup++;
                        rsDetail.add(String.valueOf(vatGroup));
                    }else rsDetail.add("null");



                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }else rsDetail.add("null");

                    //Weightable
                    detailItem = detail.findField(ECRUtil.ITEMOPTIONS);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }else rsDetail.add("null");

                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String getDepartment(int depId){

        String idStr = String.format("%02d", depId);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.DEPARTMENT}));
        message.addItem(new GMPField(ECRUtil.DEPT, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null){
            if (hResponse.getErrorNo()==0){
                System.out.println(hResponse.toString());
                GMPGroup detail = hResponse.getDetail();
                if (detail!=null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }else rsDetail.add("null");


                    detailItem = detail.findField(ECRUtil.VATGROUP_NO);
                    if (detailItem != null) {
                        String vatG = DataConvert.byteArrayToHexString(detailItem.getValue());
                        int vatGroup = Integer.parseInt(vatG);
                        vatGroup++;
                        rsDetail.add(String.valueOf(vatGroup));
                    }else rsDetail.add("null");



                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }else rsDetail.add("null");

                    //Weightable
                    detailItem = detail.findField(ECRUtil.ITEMOPTIONS);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }else rsDetail.add("null");

                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setCreditInfo(int id, String name){
        String padded = String.format("%-15s", name);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CREDIT}));
        message.addItem(new GMPField(ECRUtil.INDEX,new byte[]{(byte) id}));
        message.addItem(new GMPField(ECRUtil.NOTE,padded));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            String creditName = new String(detailItem.getValue(), "windows-1254");
                            rsDetail.add(creditName);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getCreditInfo(int id){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CREDIT}));
        message.addItem(new GMPField(ECRUtil.INDEX,new byte[]{(byte) id}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            String creditName = new String(detailItem.getValue(), "windows-1254");
                            rsDetail.add(creditName);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setCurrencyInfo(int id, String name, double exchangeRate){
        String padded = String.format("%-15s", name);
        String exRateStr = df.format(exchangeRate).replace(".", "");

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.FCURRENCY}));
        message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{(byte) id}));
        message.addItem(new GMPField(ECRUtil.NOTE,padded));
        message.addItem(new GMPField(ECRUtil.AMOUNT, DataConvert.hexStringToByteArray(exRateStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    //CURRENCY NAME
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            String curName = new String(detailItem.getValue(), "windows-1254");
                            rsDetail.add(curName);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    //CURRENCY RATE
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                      rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getCurrencyInfo(int index){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.FCURRENCY}));
        message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{(byte) index}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    //CURRENCY NAME
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            String curName = new String(detailItem.getValue(), "windows-1254");
                            rsDetail.add(curName);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    //CURRENCY RATE
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setMainCategory(int id, String name){
        String idStr = String.format("%02d", id+1);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.MAIN_CAT}));
        message.addItem(new GMPField(ECRUtil.CATEGORY_NO, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.NOTE, name));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    //DEPT NAME
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getMainCategory(int mainCatId){
        String idStr = String.format("%02d", mainCatId+1);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.MAIN_CAT}));
        message.addItem(new GMPField(ECRUtil.CATEGORY_NO, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    //DEPT NAME
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setSubCategory(int id, String name, int mainCat){
        String idStr = String.format("%02d", id+1);
        String catStr = String.format("%02d", mainCat);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SUB_CAT}));
        message.addItem(new GMPField(ECRUtil.SUBCATEGORY_NO, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.NOTE, name));
        message.addItem(new GMPField(ECRUtil.CATEGORY_NO, DataConvert.hexStringToByteArray(catStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    //Name
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    //Main Category Id
                    detailItem = detail.findField(ECRUtil.CATEGORY_NO);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getSubCategory(int subCatId){
        String idStr = String.format("%02d", subCatId+1);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SUB_CAT}));
        message.addItem(new GMPField(ECRUtil.SUBCATEGORY_NO, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    //Name
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    //Main Category Id
                    detailItem = detail.findField(ECRUtil.CATEGORY_NO);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    /*TODO
    * Care Password
    * */
    public String saveCashier(int id, String name, String password){
        String idStr = String.format("%02d", id);
        String padded = String.format("%6s", password).replace(" ", "0");

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CASHIER}));
        message.addItem(new GMPField(ECRUtil.CASHIER_ID, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.NOTE, name));
        message.addItem(new GMPField(ECRUtil.PASSWORD, DataConvert.hexStringToByteArray(padded)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    //Name
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getCashier(int cashierId){
        String idStr = String.format("%02d", cashierId);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CASHIER}));
        message.addItem(new GMPField(ECRUtil.CASHIER_ID, DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    //Name
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String signInCashier(int id, String password) {
       String idStr = String.format("%02d", id);
       GMPMessage message = new GMPMessage();
       addProInfo(message, getProcessInfo());
       message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
               new byte[]{Commands.CASHIER_LOGIN}));
       message.addItem(new GMPField(ECRUtil.CASHIER_ID,
               DataConvert.hexStringToByteArray(idStr)));
       message.addItem(new GMPField(ECRUtil.PASSWORD, password.getBytes()));
       message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

       GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));
       HuginResponse hResponse = response.gethResponse();
       if (hResponse!=null){
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
       }
       return response.getErrorMessage();
    }

    public String checkCashierIsValid(int id, String password){
        String idStr = String.format("%02d", id);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CASHIER_LOGIN}));
        message.addItem(new GMPField(ECRUtil.CASHIER_ID,
                DataConvert.hexStringToByteArray(idStr)));
        message.addItem(new GMPField(ECRUtil.PASSWORD, password));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS,new byte[]{1}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null){
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getLogo(int index){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.LOGO}));
        message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{(byte) index}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setLogo(int index, String line){
        String padded = String.format("%48s", line);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.LOGO}));
        message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{(byte) index}));
        message.addItem(new GMPField(ECRUtil.NOTE, padded));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getDateTime(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.STATUS}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(GMPUtil.TAG_OP_DATE);
                    if (detailItem != null) {
                        byte[] date = detailItem.getValue();
                        detailItem = detail.findField(GMPUtil.TAG_OP_TIME);
                        byte[] time = new byte[0];
                        if (detailItem != null) {
                            time = detailItem.getValue();
                        }

                        if (date.length > 0 && time.length > 0) {
                            String dateTime = DataConvert.commonUseDate(DataConvert.byteToDateTime(date, time));
                            rsDetail.add(dateTime);
                        }
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setDateTime(Date date, Date time){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_SET_DATETIME}));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(date)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(time)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getVATRate(int index){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.VAT}));
        message.addItem(new GMPField(ECRUtil.VATGROUP_NO,new byte[]{(byte) (index+1)}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.PERCENTAGE);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setVATRate(int index, double taxRate){
        int taxInt = (int) taxRate;
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.VAT}));

        message.addItem(new GMPField(ECRUtil.VATGROUP_NO,new byte[]{(byte) (index+1)}));
        message.addItem(new GMPField(ECRUtil.PERCENTAGE,DataConvert.intTo2Bytes(taxInt)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.PERCENTAGE);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String saveProduct(int productId, String productName, int deptId, double price,
                              int weighable, String barcode, int subCatId){
        String proStr = String.format("%06d", productId);
        String deptStr = String.format("%02d", deptId);
        String weightStr = String.format("%02d", weighable);
        String subCatStr = String.format("%02d", subCatId);
        String priceStr = df.format(price).replace(".", "");

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.PLU}));

        message.addItem(new GMPField(ECRUtil.PLU,DataConvert.hexStringToByteArray(proStr)));
        message.addItem(new GMPField(ECRUtil.NOTE,productName));
        message.addItem(new GMPField(ECRUtil.DEPT, DataConvert.hexStringToByteArray(deptStr)));
        message.addItem(new GMPField(ECRUtil.AMOUNT, DataConvert.hexStringToByteArray(priceStr)));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, DataConvert.hexStringToByteArray(weightStr)));

        if (barcode!=null && !barcode.equals(""))
            message.addItem(new GMPField(ECRUtil.BARCODE, barcode));

        message.addItem(new GMPField(ECRUtil.SUBCATEGORY_NO, DataConvert.hexStringToByteArray(subCatStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    //Dept Name
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    //Dept Id
                    detailItem = detail.findField(ECRUtil.DEPT);
                    if (detailItem != null) {
                        int dept = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        if (dept > ECRUtil.MAX_DEPARTMENT_COUNT){
                            dept = 0;
                        }else dept++;

                        rsDetail.add(String.valueOf(dept));
                    }

                    //Price
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }

                    //Weighable
                    detailItem = detail.findField(ECRUtil.ITEMOPTIONS);
                    if (detailItem != null) {
                        int wei = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(wei));
                    }

                    //Barcode
                    detailItem = detail.findField(ECRUtil.BARCODE);
                    if (detailItem != null) {
                        rsDetail.add(new String(detailItem.getValue()));
                    }

                    //SubCategory
                    detailItem = detail.findField(ECRUtil.SUBCATEGORY_NO);
                    if (detailItem != null) {
                        int subCat = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()),16);
                        rsDetail.add(String.valueOf(subCat+1));
                    }

                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String getProduct(int pluNo){
        String proStr = String.format("%06d", pluNo);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.PLU}));
        message.addItem(new GMPField(ECRUtil.PLU, DataConvert.hexStringToByteArray(proStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    //Dept Name
                    GMPItem detailItem = detail.findField(ECRUtil.NOTE);
                    if (detailItem != null) {
                        try {
                            rsDetail.add(new String(detailItem.getValue(), "windows-1254"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    //Dept Id
                    detailItem = detail.findField(ECRUtil.DEPT);
                    if (detailItem != null) {
                        int dept = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        if (dept > ECRUtil.MAX_DEPARTMENT_COUNT){
                            dept = 0;
                        }else dept++;

                        rsDetail.add(String.valueOf(dept));
                    }

                    //Price
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }

                    //Weighable
                    detailItem = detail.findField(ECRUtil.ITEMOPTIONS);
                    if (detailItem != null) {
                        int wei = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(wei));
                    }

                    //Barcode
                    detailItem = detail.findField(ECRUtil.BARCODE);
                    if (detailItem != null) {
                        rsDetail.add(new String(detailItem.getValue()));
                    }

                    //SubCategory
                    detailItem = detail.findField(ECRUtil.SUBCATEGORY_NO);
                    if (detailItem != null) {
                        int subCat = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()),16);
                        rsDetail.add(String.valueOf(subCat+1));
                    }

                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String saveGMPConnectionInfo(String ip, int port)
    {
        String[] ipStr = ip.split(".");
        String formattedIp="";
        for (String ipOne : ipStr)
            formattedIp += String.format("%03s", ipOne);

        String portStr = String.format("%06d", port);
        if (port<10000){
            portStr = String.format("%04d", port);
        }

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.TSM_IP_PORT}));

        message.addItem(new GMPField(GMPUtil.DT_HOSTLOCALIP,DataConvert.hexStringToByteArray(formattedIp)));
        message.addItem(new GMPField(ECRUtil.PORT ,DataConvert.hexStringToByteArray(portStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    /*TODO: public String LoadGraphicLogo(Image imageObj)*/

    public String getProgramOptions(int progEnum){
        String proStr = String.valueOf(progEnum+1);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.PRG_OPTIONS}));
        message.addItem(new GMPField(ECRUtil.PROPNAME, proStr));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.PROPVALUE);
                    String po;

                    if (detailItem != null) {
                        po = new String(detailItem.getValue());
                        if (progEnum==Settings.RECEIPT_LIMIT){
                            po = String.valueOf(Double.parseDouble(po)/1000);
                        }
                        rsDetail.add(po);
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String saveProgramOptions(int progEnum, String proValue) {
        String proStr = String.valueOf(progEnum);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.PRG_OPTIONS}));
        message.addItem(new GMPField(ECRUtil.PROPNAME, proStr));

        String value = proValue;
        if(progEnum==Settings.RECEIPT_LIMIT){
            String[] spValue = proValue.split(",");
            String padded = String.format("%3s", spValue[1]).replace(" ", "0");
            value = spValue[0]+padded;
        }
        message.addItem(new GMPField(ECRUtil.PROPVALUE,value));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.PROPVALUE);

                    if (detailItem != null) {
                        String po = new String(detailItem.getValue());
                        rsDetail.add(po);
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    //endregion

    //region Sale Functions

    public String printDocumentHeader(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.START_RCPT}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }


    public String printDocumentHeader(String tckn_vkn, double amount, int docType){
        String docStr = String.format("%04d", docType);
        String priceStr = df.format(amount).replace(".", "");
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.START_NF_RCPT}));
        message.addItem(new GMPField(ECRUtil.DOC_TYPE,
                DataConvert.hexStringToByteArray(docStr)));
        message.addItem(new GMPField(ECRUtil.TCKN_VKN_LOGO, tckn_vkn));
        message.addItem(new GMPField(ECRUtil.AMOUNT,DataConvert.hexStringToByteArray(priceStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printParkDocument(String plate, Date entrenceDate){
        String docStr = String.format("%04d", 5);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.START_NF_RCPT}));

        message.addItem(new GMPField(ECRUtil.DOC_TYPE,
                DataConvert.hexStringToByteArray(docStr)));

        message.addItem(new GMPField(ECRUtil.PLATE_NUM, plate));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(entrenceDate)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(entrenceDate)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printItem(int PLUNo, double quantity, double price, String name, int deptId, int weighable){
        String quantityStr = df.format(quantity).replace(".", "");
        String pluStr = String.format("%06d", PLUNo);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] totalBytes = new byte[0];

        try {
            addProWithBaos(baos, getProcessInfo(), Commands.SALE_ITEM);
            baos.write(DataConvert.intTo2Bytes(ECRUtil.SALE));

            GMPMessage message = new GMPMessage();
            message.addItem(new GMPField(ECRUtil.PLU, DataConvert.hexStringToByteArray(pluStr)));
            message.addItem(new GMPField(ECRUtil.QUANTITY,DataConvert.hexStringToByteArray(quantityStr)));

            if (price != -1){
                String priceStr = df.format(price).replace(".", "");
                message.addItem(new GMPField(ECRUtil.AMOUNT,DataConvert.hexStringToByteArray(priceStr)));
            }
            if (name != null && !name.equals("")){
                message.addItem(new GMPField(ECRUtil.NOTE, name));
            }
            if (deptId != -1){
                String dept = String.format("%02d", deptId);
                message.addItem(new GMPField(ECRUtil.DEPT, DataConvert.hexStringToByteArray(dept)));
            }
            if (weighable != -1){
                String weight = String.format("%02d", weighable);
                message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, DataConvert.hexStringToByteArray(weight)));
            }

            byte[] messageBytes = message.toByte();
            baos.write(GMPUtil.getByteLength(messageBytes.length));
            baos.write(messageBytes);
            baos.write(DataConvert.intToByte3(ECRUtil.ENDOFMSG));
            baos.write(new byte[]{1, 1});

            totalBytes = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GeneralResponse response = sendAndRead(addMessageTag(null, totalBytes, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printAdjustment(int adjustType, double amount, int percentage){
        String amountStr = df.format(amount).replace(".", "");
        String percenStr = String.format("%04d", percentage);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] totalBytes = new byte[0];

        try {
            addProWithBaos(baos, getProcessInfo(), Commands.ADJUSTMENT);
            baos.write(DataConvert.intTo2Bytes(ECRUtil.DISCOUNT));

            GMPMessage message = new GMPMessage();
            if (adjustType==AdjustType.PercentDiscount || adjustType==AdjustType.PercentFee){
                message.addItem(new GMPField(ECRUtil.PERCENTAGE,DataConvert.hexStringToByteArray(percenStr)));
            }else {
                message.addItem(new GMPField(ECRUtil.AMOUNT,DataConvert.hexStringToByteArray(amountStr)));
            }

            if (adjustType == (int)AdjustType.Fee || adjustType == (int)AdjustType.PercentFee){
                message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{1, 1}));
            }

            byte[] messageBytes = message.toByte();
            baos.write(GMPUtil.getByteLength(messageBytes.length));
            baos.write(messageBytes);
            baos.write(DataConvert.intToByte3(ECRUtil.ENDOFMSG));
            baos.write(new byte[]{1, 1});

            totalBytes = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        GeneralResponse response = sendAndRead(addMessageTag(null, totalBytes, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String correct(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.ADJUSTMENT}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String vvoid(int PLUNo, double quantity){
        String quantStr = df.format(quantity).replace(".", "");
        String pluStr = String.format("%06d", PLUNo);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] totalBytes = new byte[0];

        try {
            addProWithBaos(baos, getProcessInfo(), Commands.VOID_ITEM);
            baos.write(DataConvert.intTo2Bytes(ECRUtil.SALE));

            GMPMessage message = new GMPMessage();
            message.addItem(new GMPField(ECRUtil.PLU, DataConvert.hexStringToByteArray(pluStr)));
            message.addItem(new GMPField(ECRUtil.QUANTITY, DataConvert.hexStringToByteArray(quantStr)));

            byte[] messageBytes = message.toByte();
            baos.write(GMPUtil.getByteLength(messageBytes.length));
            baos.write(messageBytes);
            baos.write(DataConvert.intToByte3(ECRUtil.ENDOFMSG));
            baos.write(new byte[]{1, 1});

            totalBytes = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        GeneralResponse response = sendAndRead(addMessageTag(null, totalBytes, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();

                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printSubTotal(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SUBTOTAL}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                ArrayList<String> rsDetail = new ArrayList<>();
                GMPGroup detail = hResponse.getDetailWithTag(ECRUtil.TOTALS);
                if (detail != null) {
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                detail = hResponse.getDetailWithTag(ECRUtil.PAYMENT);
                if (detail != null) {
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }
                hResponse.setRs(rsDetail);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printPayment(int paymentType, int index, double paidTotal){
        String paidStr = df.format(paidTotal).replace(".", "");
        String payStr = String.format("%02d", paymentType+1);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] totalBytes = new byte[0];

        try {
            addProWithBaos(baos, getProcessInfo(), Commands.PAYMENT);
            baos.write(DataConvert.intTo2Bytes(ECRUtil.PAYMENT));

            GMPMessage message = new GMPMessage();
            message.addItem(new GMPField(ECRUtil.PAYMENT_TYPE, DataConvert.hexStringToByteArray(payStr)));
            if (paymentType == PaymentType.FCURRENCY || paymentType == PaymentType.CREDIT){
                String indexStr = String.format("%02d", index);
                message.addItem(new GMPField(ECRUtil.INDEX, DataConvert.hexStringToByteArray(indexStr)));
            }
            message.addItem(new GMPField(ECRUtil.AMOUNT, DataConvert.hexStringToByteArray(paidStr)));

            byte[] messageBytes = message.toByte();
            baos.write(GMPUtil.getByteLength(messageBytes.length));
            baos.write(messageBytes);
            baos.write(DataConvert.intToByte3(ECRUtil.ENDOFMSG));
            baos.write(new byte[]{1, 1});
            totalBytes = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        GeneralResponse response = sendAndRead(addMessageTag(null, totalBytes, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                ArrayList<String> rsDetail = new ArrayList<>();
                GMPGroup detail = hResponse.getDetailWithTag(ECRUtil.TOTALS);
                if (detail != null) {
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                detail = hResponse.getDetailWithTag(ECRUtil.PAYMENT);
                if (detail != null) {
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }
                hResponse.setRs(rsDetail);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printPaymentByEFT(double paidTotal, int installment){
        String paidStr = df.format(paidTotal).replace(".", "");
        String payStr = String.format("%02d", installment);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] totalBytes = new byte[0];

        try {
            addProWithBaos(baos, getProcessInfo(), Commands.EFT_PAYMENT);
            baos.write(DataConvert.intTo2Bytes(ECRUtil.PAYMENT));

            GMPMessage message = new GMPMessage();
            message.addItem(new GMPField(ECRUtil.AMOUNT, DataConvert.hexStringToByteArray(paidStr)));
            message.addItem(new GMPField(ECRUtil.INSTALL_NUM, DataConvert.hexStringToByteArray(payStr)));

            byte[] messageBytes = message.toByte();
            baos.write(GMPUtil.getByteLength(messageBytes.length));
            baos.write(messageBytes);
            baos.write(DataConvert.intToByte3(ECRUtil.ENDOFMSG));
            baos.write(new byte[]{1, 1});
            totalBytes = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        GeneralResponse response = sendAndRead(addMessageTag(null, totalBytes, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                ArrayList<String> rsDetail = new ArrayList<>();
                GMPGroup detail = hResponse.getDetailWithTag(ECRUtil.TOTALS);
                if (detail != null) {
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                detail = hResponse.getDetailWithTag(ECRUtil.PAYMENT);
                if (detail != null) {
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }
                hResponse.setRs(rsDetail);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String closeReceipt(boolean slipCopy){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CLOSE_RCPT}));
        if (slipCopy)
            message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{1,1}));

        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    GMPItem detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
                    if (detailItem != null) {
                        int docNo = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(docNo));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String voidReceipt(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.VOID_RCPT}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    ArrayList<String> rsDetail = new ArrayList<>();
                    GMPItem detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
                    if (detailItem != null) {
                        int docNo = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(docNo));
                    }
                    hResponse.setRs(rsDetail);
                }
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printRemarkLine(String line){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CUSTOMER_NOTE}));
        message.addItem(new GMPField(ECRUtil.NOTE, line));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printReceiptBarcode(String barcode){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.RCPT_BARCODE}));
        message.addItem(new GMPField(ECRUtil.NOTE, barcode));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }
    //endregion

    //region Reports Funtions

    public String printXReport(int copy){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.X_DAILY}));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printXPluReport(int firstPlu, int lastPlu, int copy){
        String firstStr = String.format("%06d", firstPlu);
        String lastStr = String.format("%06d", lastPlu);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.X_PLU}));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(ECRUtil.PLU, DataConvert.hexStringToByteArray(firstStr)));
        message.addItem(new GMPField(ECRUtil.PLU, DataConvert.hexStringToByteArray(lastStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            };
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printZReport(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.Z_DAILY}));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) 3}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        conn.setReadTimeOut(60000);
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));
        conn.setReadTimeOut(7000);
        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String printPeriodicZZReport(int firstZ, int lastZ, int copy, boolean detail){
        String firstStr = String.format("%06d", firstZ);
        String lastStr = String.format("%06d", lastZ);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        if (detail){
            message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                    new byte[]{Commands.Z_FM_ZZ_DETAIL}));
        }else {
            message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                    new byte[]{Commands.Z_FM_ZZ}));
        }
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(ECRUtil.ZNO, DataConvert.hexStringToByteArray(firstStr)));
        message.addItem(new GMPField(ECRUtil.ZNO, DataConvert.hexStringToByteArray(lastStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printPeriodicDateReport(Date firstDay, Date lastDay, int copy, boolean detail){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        if (detail){
            message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                    new byte[]{Commands.Z_FM_DATE_DETAIL}));
        }else {
            message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                    new byte[]{Commands.Z_FM_DATE}));
        }

        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(firstDay)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(firstDay)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(lastDay)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(lastDay)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        conn.setReadTimeOut(180*1000);
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));
        conn.setReadTimeOut(7000);
        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String printEJPeriodic(Date day, int copy){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.EJ_RCPTCOPY_DATE}));

        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(day)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(day)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String printEJPeriodic(Date stDate, Date endDate ,int copy){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.EJ_RCPTCOPY_DATE}));

        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(stDate)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(stDate)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(endDate)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(endDate)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String printEJPeriodic(int ZStartId, int docStartId, int ZEndId, int docEndId, int copy){
        return printEJReport(ZStartId, docStartId, ZEndId, docEndId, copy);
    }

    public String printEJDetail(int copy){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.EJ_DETAIL}));

        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    //endregion

    //region Service Functions
    public String enterServiceMode(String password){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.ENTER_SERVICE}));

        message.addItem(new GMPField(ECRUtil.PASSWORD, password));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String exitServiceMode(String password){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_EXIT_SERVICE}));

        message.addItem(new GMPField(ECRUtil.PASSWORD, password));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String clearDailyMemory(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_CLEAR_DAILY}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String factorySettings(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_FACTORY_SETTINGS}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String closeFM(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_STOP_FM}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String setExternalDevAddress(String ip, int port){
        String[] ipStr = ip.split(".");
        String formattedIp="";
        for (String ipOne : ipStr)
            formattedIp += String.format("%03s", ipOne);

        String portStr = String.format("%06d", port);
        if (port<10000){
            portStr = String.format("%04d", port);
        }

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_EXTERNAL_DEV_SETTINGS}));

        message.addItem(new GMPField(GMPUtil.DT_HOSTLOCALIP, DataConvert.hexStringToByteArray(formattedIp)));
        message.addItem(new GMPField(ECRUtil.PORT ,DataConvert.hexStringToByteArray(portStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String updateFirmware(String ip, int port){
        String[] ipStr = ip.split(".");
        String formattedIp="";
        for (String ipOne : ipStr)
            formattedIp += String.format("%03s", ipOne);

        String portStr = String.format("%06d", port);
        if (port<10000){
            portStr = String.format("%04d", port);
        }

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_UPDATE_FIRMWARE}));

        message.addItem(new GMPField(GMPUtil.DT_HOSTLOCALIP, DataConvert.hexStringToByteArray(formattedIp)));
        message.addItem(new GMPField(ECRUtil.PORT ,DataConvert.hexStringToByteArray(portStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String printLogs(Date date){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_PRINT_LOGS}));

        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(date)));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(date)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String createDB(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.SRV_CREATE_DB}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    //endregion

    //region Utility

    public String checkPrinterStatus(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.STATUS}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String getLastResponse(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.LAST_RESPONSE}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String cashIn(double amount){
        String amountStr = df.format(amount).replace(".", "");

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CASH_IN}));
        message.addItem(new GMPField(ECRUtil.AMOUNT, DataConvert.hexStringToByteArray(amountStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String cashOut(double amount){
        String amountStr = df.format(amount).replace(".", "");

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.CASH_OUT}));
        message.addItem(new GMPField(ECRUtil.AMOUNT, DataConvert.hexStringToByteArray(amountStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String closeNFReceipt(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.END_NF_RCPT}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String fiscalize(String password){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.FISCALIZE}));
        message.addItem(new GMPField(ECRUtil.PASSWORD, password));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String startFMTest(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.FISCALIZE}));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, DataConvert.hexStringToByteArray("01")));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String getDrawerInfo(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.INFO_DRAWER}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                ArrayList<String> rsDetail = new ArrayList<>();
                GMPGroup detail = hResponse.getDetailWithTag(ECRUtil.TOTALS);
                if (detail != null) {
                    //Total Document Count
                    GMPItem detailItem = detail.findField(ECRUtil.QUANTITY);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    //Total Amount
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                // Get total fiscal receipt group data
                detail = hResponse.getDetailWithTag(ECRUtil.SALE);
                if (detail != null) {

                    //Total fiscal receipt count
                    GMPItem detailItem = detail.findField(ECRUtil.QUANTITY);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    //Total fiscal sale amount
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                // Get total voided receipt data block
                detail = hResponse.getDetailWithTag(ECRUtil.VOID);
                if (detail != null) {

                    //Total voided document count
                    GMPItem detailItem = detail.findField(ECRUtil.QUANTITY);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    //Total voided amount
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                // Get discount data block
                detail = hResponse.getDetailWithTag(ECRUtil.DISCOUNT);
                if (detail != null) {
                    //Discount amount
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                //PAYMENT
                detail = hResponse.getDetailWithTag(ECRUtil.PAYMENT);
                if (detail != null) {

                    // Payment type Cash, Credit, Check, Foreign Currency
                    GMPItem detailItem = detail.findField(ECRUtil.PAYMENT_TYPE);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    // Sub Payment Index, 1-CreditA, 2-CreditB...
                    detailItem = detail.findField(ECRUtil.INDEX);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }

                    // Paid total
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }

                hResponse.setRs(rsDetail);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getLastDocumentInfo(boolean lastZ){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        if (lastZ){
            message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                    new byte[]{Commands.INFO_LAST_Z}));
        }else {
            message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                    new byte[]{Commands.INFO_LAST_RCPT}));
        }
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                ArrayList<String> rsDetail = new ArrayList<>();
                GMPGroup detail = hResponse.getDetailWithTag(ECRUtil.DETAIL);
                if (detail != null) {
                    //Fiş No
                    GMPItem detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
                    if (detailItem != null) {
                        int docNo = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(docNo));
                    }else rsDetail.add("null");

                    //Z NO
                    detailItem = detail.findField(ECRUtil.ZNO);
                    if (detailItem != null) {
                        int zNo = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(zNo));
                    }else rsDetail.add("null");

                    //EJ NO
                    detailItem = detail.findField(ECRUtil.EJNO);
                    if (detailItem != null) {
                        int ejNo = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(ejNo));
                    }else rsDetail.add("null");

                    //Belge Tipi
                    detailItem = detail.findField(ECRUtil.DOC_TYPE);
                    if (detailItem != null) {
                        int docType = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(docType));
                    }else rsDetail.add("null");

                    //Tarih ve Saat
                    detailItem = detail.findField(GMPUtil.TAG_OP_DATE);
                    if (detailItem != null) {
                        byte[] date = detailItem.getValue();
                        detailItem = detail.findField(GMPUtil.TAG_OP_TIME);
                        byte[] time = new byte[0];
                        if (detailItem != null) {
                            time = detailItem.getValue();
                        }

                        if (date.length > 0 && time.length > 0) {
                            String dateTime = DataConvert.commonUseDate(DataConvert.byteToDateTime(date, time));
                            rsDetail.add(dateTime);
                        }
                    }else {
                        rsDetail.add("null");
                        rsDetail.add("null");
                    }

                }else {
                    rsDetail.add("null");
                    rsDetail.add("null");
                    rsDetail.add("null");
                    rsDetail.add("null");
                    rsDetail.add("null");
                    rsDetail.add("null");
                }

                // Get total fiscal receipt group data
                detail = hResponse.getDetailWithTag(ECRUtil.TOTALS);
                if (detail != null) {

                    //Total fiscal receipt count
                    GMPItem detailItem = detail.findField(ECRUtil.QUANTITY);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    //Total fiscal sale amount
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }else {
                    rsDetail.add("null");
                    rsDetail.add("null");
                }

                // TOPLAM SATIŞ BİLGİLERİ
                detail = hResponse.getDetailWithTag(ECRUtil.SALE);
                if (detail != null) {

                    //Toplam Satış Fiş
                    GMPItem detailItem = detail.findField(ECRUtil.QUANTITY);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    //Total sale
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }else {
                    rsDetail.add("null");
                    rsDetail.add("null");
                }

                // İPTAL BİLGİLERİ
                detail = hResponse.getDetailWithTag(ECRUtil.VOID);
                if (detail != null) {

                    //Toplam İptal Fiş
                    GMPItem detailItem = detail.findField(ECRUtil.QUANTITY);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    //Total Cancelation
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }else {
                    rsDetail.add("null");
                    rsDetail.add("null");
                }

                // Get discount data block
                detail = hResponse.getDetailWithTag(ECRUtil.DISCOUNT);
                if (detail != null) {
                    //Discount amount
                    GMPItem detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }else rsDetail.add("null");

                //PAYMENT
                detail = hResponse.getDetailWithTag(ECRUtil.PAYMENT);
                if (detail != null) {

                    // Payment type Cash, Credit, Check, Foreign Currency
                    GMPItem detailItem = detail.findField(ECRUtil.PAYMENT_TYPE);
                    if (detailItem != null) {
                        int quantity = Integer.parseInt(DataConvert.byteArrayToHexString(detailItem.getValue()));
                        rsDetail.add(String.valueOf(quantity));
                    }

                    // Sub Payment Index, 1-CreditA, 2-CreditB...
                    detailItem = detail.findField(ECRUtil.INDEX);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    }

                    // Paid total
                    detailItem = detail.findField(ECRUtil.AMOUNT);
                    if (detailItem != null) {
                        rsDetail.add(doubleToStr(detailItem.getValue()));
                    }
                }else {
                    rsDetail.add("null");
                    rsDetail.add("null");
                    rsDetail.add("null");
                }

                hResponse.setRs(rsDetail);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String getServiceCode(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.START_SERVICE}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                ArrayList<String> rsDetail = new ArrayList<>();
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    //Fiş No
                    GMPItem detailItem = detail.findField(ECRUtil.INDEX);
                    if (detailItem != null) {
                        rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
                    } else rsDetail.add("null");
                }
                hResponse.setRs(rsDetail);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String interruptReport(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.BREAK}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }

    public String openDrawer(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.OPEN_DRAWER}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    //ToDo: Take a look Conditions
    public String saveNetworkSettings(String ip, String subnet, String gateway){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.NETWORK}));

        if (ip==null || ip.equals("")){
            message.addItem(new GMPField(GMPUtil.DT_HOSTLOCALIP, new byte[]{}));
            message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{0}));
        }else {
            String[] ipStr = ip.split("\\.");
            String formattedIp="";
            for (String ipOne : ipStr){
                formattedIp += String.format("%3s", ipOne).replace(' ','0');
            }
            message.addItem(new GMPField(GMPUtil.DT_HOSTLOCALIP, DataConvert.hexStringToByteArray(formattedIp)));
            message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{0}));

            if (subnet!=null && !subnet.equals("")){
                String[] subStr = subnet.split("\\.");
                String formattedSub="";
                for (String subOne : subStr)
                    formattedSub += String.format("%3s", subOne).replace(' ', '0');
                message.addItem(new GMPField(GMPUtil.DT_HOSTLOCALIP, DataConvert.hexStringToByteArray(formattedSub)));
                message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{1}));
            }

            if (gateway!=null && !gateway.equals("")){
                String[] gateStr = gateway.split("\\.");
                String formattedGate="";
                for (String gateOne : gateStr)
                    formattedGate += String.format("%3s", gateOne).replace(' ','0');
                message.addItem(new GMPField(GMPUtil.DT_HOSTLOCALIP, DataConvert.hexStringToByteArray(formattedGate)));
                message.addItem(new GMPField(ECRUtil.INDEX, new byte[]{2}));
            }
        }
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String setEJLimit(int index){
        String formated = String.format("%06d", index);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.EJ_LIMIT}));
        message.addItem(new GMPField(ECRUtil.EJ_LIMIT_LINE, DataConvert.hexStringToByteArray(formated)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));

        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String startEJ(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.INIT_EJ}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String startFM(int fiscalNo){
        String formated = String.format("%08d", fiscalNo);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.START_FM}));
        message.addItem(new GMPField(ECRUtil.REG_ID, DataConvert.hexStringToByteArray(formated)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String startNFReceipt(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.START_NF_RCPT}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    public String startNFDocument(int documentType){
        String formated = String.format("%04d", documentType);
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.START_NF_RCPT}));
        message.addItem(new GMPField(ECRUtil.DOC_TYPE, DataConvert.hexStringToByteArray(formated)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0){
                parseDetail(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    /* Todo: Transfer File not included
    public String transferFile(String fileName){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.FILE_TRANSFER}));
        message.addItem(new GMPField(ECRUtil.FILE_NAME, fileName));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {

            return hResponse.getRsDetail();
        }

        return response.getErrorMessage();
    }
    */

    public String writeNFLine(String[] lines){
        GeneralResponse response= new GeneralResponse();
        boolean isFirst = true;
        for (String line : lines){
            GMPMessage message = new GMPMessage();
            addProInfo(message, getProcessInfo());
            message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                    new byte[]{Commands.ADD_NF_LINE}));
            if (isFirst){
                byte[] lineBytes = line.getBytes();
                byte[] strBytes = ByteBuffer.allocate(lineBytes.length+2).put((byte) 0x0B)
                        .put(lineBytes).put((byte) 0x0B).array();
                message.addItem(new GMPField(ECRUtil.NOTE, strBytes));
            }else {
                message.addItem(new GMPField(ECRUtil.NOTE, line));
            }

            isFirst=false;
            message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
            response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));
        }


        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }
    //endregion

    //region Private Functions
    private String getPrinterDate(){
        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.STATUS}));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo()==0) {
                ArrayList<String> rsDetail = new ArrayList<>();
                GMPGroup detail = hResponse.getDetail();
                if (detail != null) {
                    GMPItem detailItem = detail.findField(GMPUtil.TAG_OP_DATE);
                    if (detailItem != null) {
                        byte[] date = detailItem.getValue();
                        detailItem = detail.findField(GMPUtil.TAG_OP_TIME);
                        byte[] time = new byte[0];
                        if (detailItem != null) {
                            time = detailItem.getValue();
                        }

                        if (date.length > 0 && time.length > 0) {
                            String dateTime = DataConvert.commonUseDate(DataConvert.byteToDateTime(date, time));
                            rsDetail.add(dateTime);
                        }
                    }
                }
                hResponse.setRs(rsDetail);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    private String printEJReport(int firstZNo, int firstDocId, int lastZNo, int lastDocId, int copy){
        String zStr = String.format("%06d", firstZNo);
        String fDocStr = String.format("%06d", firstDocId);
        String lDocStr = String.format("%06d", lastDocId);
        String lZStr = String.format("%06d", lastZNo);

        GMPMessage message = new GMPMessage();
        addProInfo(message, getProcessInfo());
        message.addItem(new GMPField(ECRUtil.FPU_FISCAL_COMMAND,
                new byte[]{Commands.STATUS}));
        message.addItem(new GMPField(ECRUtil.ITEMOPTIONS, new byte[]{(byte) copy}));
        message.addItem(new GMPField(ECRUtil.ZNO, DataConvert.hexStringToByteArray(zStr)));
        message.addItem(new GMPField(ECRUtil.DOCUMENT_NUM, DataConvert.hexStringToByteArray(fDocStr)));
        message.addItem(new GMPField(ECRUtil.ZNO, DataConvert.hexStringToByteArray(lZStr)));
        message.addItem(new GMPField(ECRUtil.DOCUMENT_NUM, DataConvert.hexStringToByteArray(lDocStr)));
        message.addItem(new GMPField(ECRUtil.ENDOFMSG, new byte[]{1}));
        GeneralResponse response = sendAndRead(addMessageTag(message, null, REQUEST_MSG_ID));

        HuginResponse hResponse = response.gethResponse();
        if (hResponse!=null) {
            if (hResponse.getErrorNo() == 0) {
                parseReport(hResponse);
            }
            return hResponse.getRsDetail();
        }
        return response.getErrorMessage();
    }

    //endregion

    public HuginResponse parse(byte[] buffer){
        ByteBuffer bbf = ByteBuffer.wrap(buffer,2,buffer.length-2);
        byte[] terminalBytes = new byte[12];
        byte[] messageTag = new byte[3];
        bbf.get(terminalBytes);
        System.out.println("Terminal No -> " + new String(terminalBytes).trim());
        bbf.get(messageTag);
        String messTagStr = "";
        for (byte by : messageTag) {
            messTagStr += String.format("%02x", by & 0xff);
        }
        System.out.println("Message Tag " + messTagStr);
        int messTag = DataConvert.byte3ToInt(messageTag);
        if (messTag != RESPONSE_MSG_ID){
            try {
                throw new Exception("Response Message Incorrect");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int msgLen = GMPUtil.getLength(bbf);
        byte[] encrypted = new byte[msgLen];
        bbf.get(encrypted);
        byte[] decrypted = keyContext.decData(encrypted);
        int padLen = decrypted[0];
        byte[] clear =Arrays.copyOfRange(decrypted, padLen, decrypted.length);
        ByteBuffer bbf2 = ByteBuffer.wrap(clear);

        byte[] tagBytes = new byte[GMPUtil.LEN_DATA_TAG];
        int length;
        byte[] value;
        HuginResponse response = new HuginResponse();

        while (bbf2.hasRemaining()) {
            bbf2.get(tagBytes);
            int tag = DataConvert.byte3ToInt(tagBytes);
            switch (tag){
                case GMPUtil.TAG_SEQUNCE:
                    length = GMPUtil.getLength(bbf2);
                    value = new byte[length];
                    bbf2.get(value);
                    String seqNum = DataConvert.byteArrayToHexString(value);
                    response.setSeqNumber(seqNum);
                    break;
                case ECRUtil.ERROR:
                    length = GMPUtil.getLength(bbf2);
                    value = new byte[length];
                    bbf2.get(value);
                    response.setError(value[0]);
                    break;
                case ECRUtil.STATE:
                    length = GMPUtil.getLength(bbf2);
                    value = new byte[length];
                    bbf2.get(value);
                    String state = DataConvert.byteArrayToHexString(value);
                    response.setState(Integer.parseInt(state));
                    break;

                case ECRUtil.ENDOFMSG:
                    length = GMPUtil.getLength(bbf2);
                    value = new byte[length];
                    bbf2.get(value);
                    break;

                default:
                    bbf2.position(bbf2.position() - 3);
                    byte[] groupTag = new byte[2];
                    bbf2.get(groupTag);
                    GMPGroup detail = new GMPGroup(DataConvert.byte2ToInt(groupTag));
                    int groupLen = GMPUtil.getLength(bbf2);
                    int end = bbf2.position()+groupLen;
                    while (bbf2.position()<end){
                        bbf2.get(tagBytes);
                        int grTag = DataConvert.byte3ToInt(tagBytes);
                        int grLength = GMPUtil.getLength(bbf2);
                        byte[] grValue = new byte[grLength];
                        bbf2.get(grValue);
                        GMPField field = new GMPField(grTag, grValue);
                        detail.addField(field);
                    }
                    response.addDetail(detail);
                    break;
            }

        }
        return response;
    }

    private void parseReport(HuginResponse response){
        GMPGroup detail = response.getDetail();
        if (detail!=null) {
            ArrayList<String> rsDetail = new ArrayList<>();

            GMPItem detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
            if (detailItem != null) {
                int docNo;
                try {
                    docNo = Integer.parseInt(DataConvert.byteArrayToHexString(detail.getValue()));
                }catch (Exception e){
                    docNo = -1;
                }
                rsDetail.add(String.valueOf(docNo));
            } else rsDetail.add("null");

            detailItem = detail.findField(ECRUtil.ZNO);
            if (detailItem != null) {
                int zNo;
                try {
                    zNo = Integer.parseInt(DataConvert.byteArrayToHexString(detail.getValue()));
                }catch (Exception e){
                    zNo = -1;
                }
                rsDetail.add(String.valueOf(zNo));
            } else rsDetail.add("null");
            response.setRs(rsDetail);
        }
    }

    private void parseDetail(HuginResponse response){
        GMPGroup detail = response.getDetail();
        if (detail!=null) {
            ArrayList<String> rsDetail = new ArrayList<>();

            GMPItem detailItem = detail.findField(GMPUtil.TAG_OP_DATE);
            if (detailItem != null) {
                byte[] date = detailItem.getValue();
                detailItem = detail.findField(GMPUtil.TAG_OP_TIME);
                byte[] time = new byte[0];
                if (detailItem != null) {
                    time = detailItem.getValue();
                }

                if (date.length > 0 && time.length > 0) {
                    String dateTime = DataConvert.commonUseDate(DataConvert.byteToDateTime(date, time));
                    rsDetail.add(dateTime);
                }
            }else {
                rsDetail.add("null");
                rsDetail.add("null");
            }

            detailItem = detail.findField(ECRUtil.NOTE);
            if (detailItem != null) {
                try {
                    rsDetail.add(new String(detailItem.getValue(),"windows-1254"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }else rsDetail.add("null");

            detailItem = detail.findField(ECRUtil.AMOUNT);
            if (detailItem != null) {
                rsDetail.add(doubleToStr(detailItem.getValue()));
            }else rsDetail.add("null");

            detailItem = detail.findField(ECRUtil.DOCUMENT_NUM);
            if (detailItem != null) {
                rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
            }else rsDetail.add("null");

            detailItem = detail.findField(ECRUtil.INDEX);
            if (detailItem != null) {
                rsDetail.add(DataConvert.byteArrayToHexString(detailItem.getValue()));
            }else rsDetail.add("null");

            detailItem = detail.findField(ECRUtil.CASHIER_AUTH);
            if (detailItem != null) {
                int authLevel = detailItem.getValue()[0];
                String auth;
                switch (authLevel){
                    case 0: auth="SATIŞ"; break;
                    case 1: auth="PROGRAM"; break;
                    case 2: auth="SATIŞ & Z"; break;
                    case 3: auth="HEPSİ"; break;
                    default: auth = String.valueOf(authLevel);
                        break;
                }
                rsDetail.add(auth);
            }
            response.setRs(rsDetail);
        }
    }

    private byte[] addMessageTag(GMPMessage message, byte[] totalBytes, int tag){
        byte[] beforeEncr;
        if (message==null){
            beforeEncr = totalBytes;
        }else {
            beforeEncr = message.toByte();
        }
        byte[] msgBytes =  keyContext.encryptMessage(beforeEncr, new byte[0]);
        byte[] msgTag = DataConvert.intToByte3(tag);
        byte[] pack = new byte[0];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            String padded = String.format("%12s", "FP00001849");
            baos.write(padded.getBytes());
            baos.write(msgTag);
            baos.write(GMPUtil.getByteLength(msgBytes.length));
            baos.write(msgBytes);
            byte[] msgLength = DataConvert.intTo2Bytes(baos.size());
            byte[] crc = CRC16.CRC16CITT(baos.toByteArray());
            baos.write(crc);
            byte[] current = baos.toByteArray();
            baos.reset();
            baos.write(GMPUtil.STX);
            baos.write(msgLength);
            baos.write(current);
            baos.write(GMPUtil.ETX);
            pack = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pack;
    }

}
