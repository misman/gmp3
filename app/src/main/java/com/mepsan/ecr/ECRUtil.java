package com.mepsan.ecr;

/**
 * Created by misman on 14.10.2015.
 */
public class ECRUtil {

    private static String[] StateMessages =
            {
                    "BEKLEMEDE",            /*ST_IDLE*/
                    "SATIŞ İÇİNDE",         /*ST_SELLING*/
                    "ARATOPLAM",            /*ST_SUBTOTAL*/
                    "ÖDEME BAŞLADI",        /*ST_PAYMENT*/
                    "FİŞ KAPAT",            /*ST_OPEN_SALE*/
                    "BİLGİ FİŞİ",           /*ST_INFO_RCPT*/
                    "ÖZEL FİŞ",             /*ST_CUSTOM_RCPT*/
                    "SERVİS BAŞLADI",       /*ST_IN_SERVICE*/
                    "SERVİS GEREKLİ",       /*ST_SRV_REQUIRED*/
                    "KASİYER GİRİŞİ",       /*ST_LOGIN*/
                    "MALİ MODDA DEĞİL",     /*ST_NONFISCAL*/
                    "ELEKTRİK KESİNTİSİ",   /*ST_ON_PWR_RCOVR*/
                    "FATURA BAŞLADI",       /*ST_INVOICE*/
                    "ONAY BEKLEMEKTE",      /*ST_CONFIRM_REQUIRED*/
            };
    private static String[][] ErrorMessages =
    {
            /*ErrorCode , Message*/
        {"-1","AYRIŞTIRMA HATASI"},
        {"0","İŞLEM BAŞARILI"},
        {"1","VERİ EKSİK GELMİŞ (UZUNLUK KADAR GELMELİ)"},
        {"2","VERİ DEĞİŞMİŞ"},
        {"3","UYGULAMA DURUMU UYGUN DEĞİL"},
        {"4","BÖYLE BİR KOMUT DESTEKLENMİYOR"},
        {"5","PARAMETRE GEÇERSİZ"},
        {"6","OPERASYON BAŞARISIZ"},
        {"7","SİLME GEREKLİ (HATA SONRASI)"},
        {"8","KAĞIT YOK"},
        {"9","CİHAZ EŞLEME YAPILAMADI."},
        {"11","MALİ BELLEK BİLGİLERİ ALINIRKEN HATA OLUŞTU"},
        {"12","MALİ BELLEK TAKILI DEĞİL"},
        {"13","MALİ BELLEK UYUMSUZLUĞU"},
        {"14","MALİ BELLEK FORMATLANMALI"},
        {"15","MALİ BELLEK FORMATLANIRKEN HATA OLUŞTU"},
        {"16","MALİ BELLEK MALİLEŞTİRME YAPILAMADI"},
        {"17","GÜNLÜK Z LİMİT"},
        {"18","MALİ BELLEK DOLDU"},
        {"19","MALİ BELLEK DAHA ÖNCE FORMATLANMIŞ"},
        {"20","MALİ BELLEK KAPATILMIŞ"},
        {"21","GEÇERSİZ MALİ BELLEK"},
        {"22","SERTİFİKALAR YÜKLENEMEDİ"},
        {"31","EKÜ BİLGİLERİ ALINIRKEN HATA OLUŞTU"},
        {"32","EKÜ ÇIKARILDI"},
        {"33","EKÜ KASAYA AİT DEĞİL"},
        {"34","ESKİ EKÜ (SADECE EKÜ RAPORLARI)"},
        {"35","YENİ EKÜ TAKILDI, ONAY BEKLİYOR"},
        {"36","EKÜ DEĞİŞTİRİLEMEZ, Z GEREKLİ"},
        {"37","YENİ EKÜYE GEÇİLEMİYOR"},
        {"38","EKÜ DOLDU, Z GEREKLİ"},
        {"39","EKÜ DAHA ÖNCE FORMATLANMIŞ"},
        {"51","FİŞ LİMİTİ AŞILDI"},
        {"52","FİŞ KALEM ADEDİ AŞILDI"},
        {"53","SATIŞ İŞLEMİ GEÇERSİZ"},
        {"54","İPTAL İŞLEMİ GEÇERSİZ"},
        {"55","DÜZELTME İŞLEMİ YAPILAMAZ"},
        {"56","İNDİRİM/ARTIRIM İŞLEMİ YAPILAMAZ"},
        {"57","ÖDEME İŞLEMİ GEÇERSİZ"},
        {"58","ASGARİ ÖDEME SAYISI AŞILDI"},
        {"59","GÜNLÜK ÜRÜN SATIŞI AŞILDI"},
        {"71","KDV ORANI TANIMSIZ"},
        {"72","KISIM TANIMLANMAMIŞ"},
        {"73","TANIMSIZ ÜRÜN"},
        {"74","KREDİLİ ÖDEME BİLGİSİ EKSİK/GEÇERSİZ"},
        {"75","DÖVİZLİ ÖDEME BİLGİSİ EKSİK/GEÇERSİZ"},
        {"76","EKÜDE KAYIT BULUNAMADI"},
        {"77","MALİ BELLEKTE KAYIT BULUNAMADI"},
        {"78","ALT ÜRÜN GRUBU TANIMLI DEĞİL"},
        {"79","DOSYA BULUNAMADI"},
        {"91","KASİYER YETKİSİ YETERSİZ"},
        {"92","SATIŞ VAR"},
        {"93","SON FİŞ Z DEĞİL"},
        {"94","KASADA YETERLİ PARA YOK"},
        {"95","GÜNLÜK FİŞ SAYISI LİMİT AŞILDI"},
        {"96","GÜNLÜK TOPLAM AŞILDI"},
        {"97","KASA MALİ DEĞİL"},
        {"111","SATIR UZUNLUĞU BEKLENENDEN FAZLA"},
        {"112","KDV ORANI GEÇERSİZ"},
        {"113","DEPT NUMARASI GEÇERSİZ"},
        {"114","PLU NUMARASI GEÇERSİZ"},
        {"115","GEÇERSİZ TANIM (ÜRÜN ADI, KISIM ADI, KREDİ ADI...VS)"},
        {"116","BARKOD GEÇERSİZ"},
        {"117","GEÇERSİZ OPSİYON"},
        {"118","TOPLAM TUTMUYOR"},
        {"119","GEÇERSİZ MİKTAR"},
        {"120","GEÇERSİZ TUTAR"},
        {"121","MALİ NUMARA HATALI"},
        {"131","KAPAKLAR AÇILDI"},
        {"132","MALİ BELLEK MESH ZARAR VERİLDİ"},
        {"133","HUB MESH ZARAR VERİLDİ"},
        {"134","Z ALINMALI(24 SAAT GEÇTİ)"},
        {"135","DOĞRU EKÜ TAK, YENİDEN BAŞLAT"},
        {"136","SERTİFİKA YÜKLENEMEDİ"},
        {"137","TARİH-SAAT AYARLAYIN"},
        {"138","GÜNLÜK İLE MALİ BELLEK UYUMSUZ"},
        {"139","VERİTABANI HATASI"},
        {"140","LOG HATASI"},
        {"141","SRAM HATASI"},
        {"142","SERTİFİKA UYUMSUZ"},
        {"143","VERSİYON HATASI"},
        {"144","GÜNLÜK LOG SAYISI AŞILDI"},
        {"145","YAZARKASAYI YENİDEN BAŞLAT"},
        {"146","KASİYER/SERVİS GÜNLÜK YANLIŞ ŞİFRE GİRİŞİ SAYISINI AŞTI"},
        {"147","MALİLEŞTİRME YAPILDI. YENİDEN BAŞLAT"},
        {"148","GİB'e BAĞLANILAMADI. TEKRAR DENE(İŞLEM DURDURMA)"},
        {"149","SERTİFİKA İNDİRİLDİ. YENİDEN BAŞLAT"},
        {"150","GÜVENLİ ALAN FORMATLANAMADI"},
        {"151","JUMPER ÇIKART TAK"}
    };
    public static String GetErrorMessage(int errorCode)
    {
        String error = String.valueOf(errorCode);
        for (int i=0; i<ErrorMessages.length; i++){
            if (ErrorMessages[i][0].equals(error)){
                return ErrorMessages[i][1];
            }
        }
        return "Hata Bulunamadı";
    }
    public static String GetStateMessage(int state)
    {
        return StateMessages[state-1];
    }

    //Program Config
    public static final int LOGO_LINE_LENGTH = (48);
    public static final int PRODUCT_NAME_LENGTH = (20);
    public static final int PLU_NAME_FIXLENGTH = (15);
    public static final int CREDIT_NAME_LENGTH = (15);
    public static final int FCURRENCY_NAME_LENGTH = (15);
    public static final int DEP_NAME_LENGTH = (20);
    public static final int LENGTH_OF_LOGO_LINES = 6;
    public static final int MAX_CREDIT_COUNT = 8;
    public static final int MAX_FCURRENCY_COUNT = 4;
    public static final int MAX_VAT_RATE_COUNT = 8;
    public static final int MAX_DEPARTMENT_COUNT = 8;
    public static final int MAX_SUB_CAT_COUNT = 250;
    public static final int MAX_MAIN_CATEGORY_COUNT = 50;
    public static final int MAX_CASHIER_COUNT = 10;

    public static final int AMOUNT_LENGTH = 5;

    public static final int FPU_CMD_TIMEOUT = 15000;
    public static final int FPU_RPRT_TIMEOUT = 45000;
    public static final int FPU_SRV_TIMEOUT = 60000;
    public static final int FPU_START_FM_TIMEOUT = 120000;

    //FPU COMMON TAG
    public static final int FPU_FISCAL_COMMAND = 0xDFF021;

    //FPU GROUP TAGS
    public static final int DETAIL = 0xDF40;
    public static final int SALE = 0xDF71;
    public static final int VOID = 0xDF72;
    public static final int TOTALS = 0xDF73;
    public static final int PAYMENT = 0xDF74;
    public static final int END = 0xDF75;
    public static final int FILE = 0xDF76;
    public static final int DISCOUNT = 0xDF77;
    public static final int NOTES = 0xDF78;
    public static final int PARAMS = 0xDF79;

    //FPU DATA TAGS
    public static final int DEPT = 0xDFF001; // Departman numarası
    public static final int PLU = 0xDFF002; // Plu Numarası
    public static final int QUANTITY = 0xDFF003; // Miktar/Count
    public static final int AMOUNT = 0xDFF004; // Fiyat/Price
    public static final int PAYMENT_TYPE = 0xDFF005; // Ödeme Tipi
    public static final int PART_NUM = 0xDFF006; // Blok no(büyük mesajlar için)
    public static final int TOTAL_PART = 0xDFF007; // Toplam blok sayısı
    public static final int FILE_NAME = 0xDFF008; // Dosya adı
    public static final int DOC_TYPE = 0xDFF009; // Belge Tipi (Fiş, Fatura,...)
    public static final int REG_ID = 0xDFF00A; // Kasaya özel numara
    public static final int CASHIER_ID = 0xDFF00B; // Kasiyer numarası
    public static final int BARCODE = 0xDFF00C; // Barkod bilgisi (Ürün barkodu...)
    public static final int DOCUMENT_NUM = 0xDFF00D; // Fiş numarası
    public static final int DOC_SERIAL = 0xDFF00E; // Seri no (Örn: Fatura seri no)
    public static final int INSTALL_NUM = 0xDFF00F; // Taksit
    public static final int INDEX = 0xDFF010; // İndeks (Kredi no,logo no)
    public static final int PERCENTAGE = 0xDFF011; // Yüzde (indirim, kdv)
    public static final int ENDOFMSG = 0xDFF012; // Paket sonu
    public static final int PAY_REFCODE = 0xDFF013; // Ödeme referans kodu
    public static final int VATGROUP_NO = 0xDFF014; // Kdv grubu numarası
    public static final int CATEGORY_NO = 0xDFF015; // Ana ürün grubu numarası
    public static final int SUBCATEGORY_NO = 0xDFF016; // Alt ürün grubu numarası
    public static final int LAST_PART = 0xDFF017; // Son paket olduğunu gösterir
    public static final int NOTE = 0xDFF018; // Metin alanı (ürün adı, satır vs)
    public static final int SALE_REFCODE = 0xDFF019; // Satış kalemine varsa harici ref kodu.
    public static final int PASSWORD = 0xDFF01A; // Şifre
    public static final int ZNO = 0xDFF01B; // Z Numarası
    public static final int EJNO = 0xDFF01C; // Ekü Numarası
    public static final int PROPNAME = 0xDFF01D; // Özellik Adı
    public static final int PROPVALUE = 0xDFF01E; // Özellik Değeri
    public static final int ITEMOPTIONS = 0xDFF01F; // Tartılabilirlik,  Fiyatlı Satış gibi departman ve ürün opsiyonları
    public static final int CMD = 0xDFF021; // Komut numarası/kodu
    public static final int ERROR = 0xDFF022; // Mali uygulamadan dönen hata kodu
    public static final int STATE = 0xDFF023; // Mali uygulamanın durumu
    public static final int PORT = (0xDFF024); // Port Numarası
    public static final int EJ_LIMIT_LINE = (0xDFF025); // Ekü limit satır sayısı
    public static final int CASHIER_AUTH = (0xDFF026); // Kasiyer yetki seviyesi
    public static final int GRAPHIC_LOGO = (0xDFF027); // Grafik Logo
    public static final int TCKN_VKN_LOGO = (0xDFF028); // Müşteri tc no veya vergi no
    public static final int PLATE_NUM = (0xDFF029); // Araç plaka


}
