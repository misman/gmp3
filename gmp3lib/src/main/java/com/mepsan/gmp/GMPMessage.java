package com.mepsan.gmp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by misman on 13.10.2015.
 */
public class GMPMessage {

    private int msgType;
    private ArrayList<GMPItem> items;


    public GMPMessage(){
        items = new ArrayList<>();
    }

    public GMPMessage(int msgType) {
        items = new ArrayList<>();
        this.msgType = msgType;
    }

    public void addItem(GMPItem item){
        items.add(item);
    }

    public void addAll(ArrayList<GMPItem> subItems){
        items.addAll(subItems);
    }

    public void insertItem(int index, GMPItem item){
        items.add(index,item);
    }

    public GMPGroup findGroup(int groupTag){
        for (GMPItem item : items){
            if(groupTag==item.getTag())
                return (GMPGroup) item;
        }

        return null;
    }

    public GMPField findField(int fieldTag){
        for (GMPItem item : items){
            if(fieldTag==item.getTag())
                return (GMPField) item;
        }

        return null;
    }


    public byte[] toByte(){
        byte[] result = new byte[0];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (GMPItem item : items){
            byte[] tag = DataConvert.intToByte3(item.getTag());
            try {
                baos.write(tag);
                baos.write(item.getLength());
                baos.write(item.getValue());
                result = baos.toByteArray();
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return result;
    }

    public int getMsgType() {
        return msgType;
    }
}
