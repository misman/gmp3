package com.mepsan.gmp;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by misman on 12.10.2015.
 */
public class GMPGroup implements GMPItem {

    private int tag;
    private ArrayList<GMPField> fields;

    public GMPGroup(int tag) {
        this.tag = tag;
        this.fields = new ArrayList<>();
    }

    @Override
    public int getTag() {
        return tag;
    }

    @Override
    public byte[] getLength() {
        int length = 0;
        for (GMPField field : fields){
            length+= GMPUtil.LEN_DATA_TAG + field.getLength().length
                    + field.getValue().length;
        }
        return GMPUtil.getByteLength(length);
    }

    private int getIntLength(){
        int length = 0;
        for (GMPField field : fields){
            length+= GMPUtil.LEN_DATA_TAG + field.getLength().length
                    + field.getValue().length;
        }
        return length;
    }

    @Override
    public byte[] getValue() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(getIntLength());
        for (GMPField field : fields){
            byteBuffer.put(DataConvert.intToByte3(field.getTag()));
            byteBuffer.put(field.getLength());
            byteBuffer.put(field.getValue());
        }
        return byteBuffer.array();
    }

    public ArrayList<GMPField> getFields() {
        return fields;
    }

    public void addField(GMPField field){
        fields.add(field);
    }

    public GMPField findField(int tag){
        for (GMPField field : fields){
            if (tag == field.getTag()){
                return field;
            }
        }

        return null;
    }
}
