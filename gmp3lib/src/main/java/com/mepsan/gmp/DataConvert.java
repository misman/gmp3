package com.mepsan.gmp;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Celalettin on 5.2.2015.
 */
public class DataConvert {

    private static final SimpleDateFormat sdfDate = new SimpleDateFormat("yyMMdd",new Locale("tr"));
    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss",new Locale("tr"));
    private static final SimpleDateFormat prsDateTime = new SimpleDateFormat("yyMMddHHmmss",new Locale("tr"));
    private static final SimpleDateFormat sdfDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm",new Locale("tr"));

    public static Date byteToDateTime(byte[] date,byte[] time){
        String dateStr = DataConvert.byteArrayToHexString(date);
        String timeStr = DataConvert.byteArrayToHexString(time);

        try {
            return prsDateTime.parse(dateStr+timeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String commonUseDate(Date date){
        return sdfDateTime.format(date);
    }

    public static byte[] dateToBCD(Date date){
        String dateString = sdfDate.format(date);
        return hexStringToByteArray(dateString);
    }

    public static byte[] timeToBCD(Date date){
        String timeString = sdfTime.format(date);
        return hexStringToByteArray(timeString);
    }

    public static byte[] dateTimeToBCD(Date date){
        byte[] bytes = new byte[14];
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        byteBuffer.put(intToByte3(GMPUtil.TAG_OP_DATE));
        byteBuffer.put((byte)3);
        byteBuffer.put(dateToBCD(date));
        byteBuffer.put(intToByte3(GMPUtil.TAG_OP_TIME));
        byteBuffer.put((byte)3);
        byteBuffer.put(timeToBCD(date));
        return byteBuffer.array();
    }
    public static byte[] hexStringToByteArray(String s){

        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
    public static String byteArrayToHexString(byte[] a) {

        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a){
            sb.append(String.format("%02x", b & 0xff));
        }

        return sb.toString().toUpperCase();
    }

    public static int byte3ToInt(byte[] bytes){
            return (bytes[0] & 0xff) << 16 | (bytes[1] & 0xff) << 8 | (bytes[2] & 0xff);
    }

    public static int byte2ToInt(byte[] bytes){
        return ((bytes[0] & 0xff) << 8) | (bytes[1] & 0xff);
    }

    public static byte[] intToByte3(int hexData)
    {
        byte[] byte3 = new byte[3];
        byte3[0] = (byte)(hexData >> 16);
        byte3[1] = (byte)(hexData >> 8);
        byte3[2] = (byte)(hexData);

        return byte3;

    }

    public static byte[] intTo2Bytes(int value){
        byte[] data = new byte[2];
        data[0] = (byte)((value >> 8) & 0xFF);
        data[1] = (byte)(value & 0xFF);

        return  data;
    }

    public static byte[] bigToBA(BigInteger bigInt){
        byte[] bigBytes = bigInt.toByteArray();
        if (bigBytes[0] == 0) {
            byte[] tmp = new byte[bigBytes.length - 1];
            System.arraycopy(bigBytes, 1, tmp, 0, tmp.length);
            bigBytes = tmp;
        }
        return bigBytes;

    }

}
