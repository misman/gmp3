package com.mepsan.match;

import com.mepsan.gmp.DataConvert;

import java.util.Date;

/**
 * Created by misman on 28.10.2015.
 */
public class ProcessInfo {

    private Date date;
    private String processId;

    public ProcessInfo(String proId) {
        processId = proId;
        date = new Date();
    }

    public ProcessInfo(Date date, String processId) {
        this.date = date;
        this.processId = processId;
    }

    public Date getDate() {
        return date;
    }

    public String getProcessId() {
        return processId;
    }

    public byte[] getProByte(){
        return DataConvert.hexStringToByteArray(processId);
    }
}
