package com.mepsan.match;

import com.mepsan.gmp.DataConvert;
import com.mepsan.gmp.GMPField;
import com.mepsan.gmp.GMPMessage;
import com.mepsan.gmp.GMPUtil;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;

/**
 * Created by misman on 28.10.2015.
 */
public class InitState extends SyncState {

    public InitState(KeyContext kContext, ProcessInfo rqIn) {
        super(kContext, rqIn);
    }

    private byte[] random16(){
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[16];
        random.nextBytes(bytes);
        return bytes;
    }

    @Override
    public byte[] getMessage() {
        byte[] randomEx = random16();
        getKeyContext().setExRandom(randomEx);
        GMPMessage message = new GMPMessage();
        MatchManager.addProInfo(message, getRqInfo());
        MatchManager.addExInfo(message, getKeyContext().getExInfo());
        message.addItem(new GMPField(GMPUtil.DT_DRIVER_VERSION, getKeyContext().getExInfo().getVersion()));
        message.addItem(new GMPField(GMPUtil.DT_RANDOM_NUMBER, randomEx));
        byte[] pack = MatchManager.addMessageTag(message,GMPUtil.MATCH_INIT_REQ);
        setPack(pack);
        return pack;
    }

    public void decompose(GMPMessage message){

        GMPField customField;
        customField = message.findField(GMPUtil.TAG_SEQUNCE);
        String proIdStr = DataConvert.byteArrayToHexString(customField.getValue());

        customField = message.findField(GMPUtil.TAG_OP_DATE);
        byte[] date = customField.getValue();

        customField = message.findField(GMPUtil.TAG_OP_TIME);
        byte[] time = customField.getValue();

        Date dateTime = DataConvert.byteToDateTime(date,time);
        setRsInfo(new ProcessInfo(dateTime,proIdStr));

        customField = message.findField(GMPUtil.DT_ECR_BRAND);
        String ecrBrand = new String(customField.getValue());

        customField = message.findField(GMPUtil.DT_ECR_MODEL);
        String ecrModel = new String(customField.getValue());

        customField = message.findField(GMPUtil.DT_ECR_SERIAL);
        String ecrSerial = new String(customField.getValue());

        customField = message.findField(GMPUtil.DT_DRIVER_VERSION);
        String version = new String(customField.getValue());

        getKeyContext().setEcrInfo(new DeviceInfo(ecrBrand, ecrModel, ecrSerial, version));

        customField = message.findField(GMPUtil.DT_ECR_CERT);
        byte[] certByte = customField.getValue();
        boolean isValid = getKeyContext().isCertValid(certByte);

        if(isValid) {
            customField = message.findField(GMPUtil.DT_DH_P);
            BigInteger p = new BigInteger(DataConvert.byteArrayToHexString(customField.getValue()), 16);

            customField = message.findField(GMPUtil.DT_DH_G);
            BigInteger g = new BigInteger(DataConvert.byteArrayToHexString(customField.getValue()), 16);

            customField = message.findField(GMPUtil.DT_ERROR_CODE);
            String errorCode = DataConvert.byteArrayToHexString(customField.getValue());
            setErrorCode(errorCode);

            customField = message.findField(GMPUtil.DT_ECR_POS_INDEX);
            String exIndex = DataConvert.byteArrayToHexString(customField.getValue());

            customField = message.findField(GMPUtil.DT_ECR_RANDOM_NUMBER);
            byte[] randomEcr = customField.getValue();

            getKeyContext().setParams(p, g, exIndex, randomEcr);
            getKeyContext().dhGeneratePublic();

        }else {
            try {
                throw new Exception("Certificate Validation Failed");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
