package com.mepsan.match;

import com.mepsan.ecr.Connection;
import com.mepsan.gmp.CRC16;
import com.mepsan.gmp.DataConvert;
import com.mepsan.gmp.GMPField;
import com.mepsan.gmp.GMPMessage;
import com.mepsan.gmp.GMPUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by misman on 20.10.2015.
 */
public class MatchManager {

    private KeyContext keyContext;
    private int currentState=0;
    private int processId=0;
    private Connection conn;

    public MatchManager(int proId, DeviceInfo exDevice, Connection connection) {
        this.keyContext = new KeyContext(exDevice);
        this.processId = proId;
        this.conn = connection;
    }

    public boolean match(){
        conn.open();
        SyncState currentState;
        while ((currentState=getNextState())!=null){
            if(conn.sendData(currentState.getMessage())) {
                try {
                    Thread.sleep(200);
                    byte[] result = conn.read();
                    if (result.length == 0) {
                        conn.close();
                        return false;
                    }
                    currentState.decompose(GMPUtil.parseData(result));
                    if(!currentState.getErrorCode().equals("0000")){
                        System.err.println("Answer Error From Ecr is " + currentState.getErrorCode());
                        conn.close();
                        return false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
                conn.close();
                System.out.println("Connection Error");
                System.out.println("Write Error");
                return false;
            }
        }
        return true;
    }

    public static byte[] addMessageTag(GMPMessage message, int tag){
        byte[] msgBytes = message.toByte();
        byte[] msgTag = DataConvert.intToByte3(tag);
        byte[] pack = new byte[0];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            String padded = String.format("%12s", KeyContext.TERMINAL_NO);
            baos.write(padded.getBytes());
            baos.write(msgTag);
            baos.write(GMPUtil.getByteLength(msgBytes.length));
            baos.write(msgBytes);
            byte[] msgLength = DataConvert.intTo2Bytes(baos.size());
            byte[] crc = CRC16.CRC16CITT(baos.toByteArray());
            baos.write(crc);
            byte[] current = baos.toByteArray();
            baos.reset();
            baos.write(GMPUtil.STX);
            baos.write(msgLength);
            baos.write(current);
            baos.write(GMPUtil.ETX);

            pack = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pack;
    }


    public static void addEcrInfo(GMPMessage message, DeviceInfo ecr){
        message.addItem(new GMPField(GMPUtil.DT_ECR_BRAND, ecr.getBrand()));
        message.addItem(new GMPField(GMPUtil.DT_ECR_MODEL, ecr.getModel()));
        message.addItem(new GMPField(GMPUtil.DT_ECR_SERIAL, ecr.getSerial()));
    }

    public static void addExInfo(GMPMessage message, DeviceInfo ex){
        message.addItem(new GMPField(GMPUtil.DT_EX_BRAND, ex.getBrand()));
        message.addItem(new GMPField(GMPUtil.DT_EX_MODEL, ex.getModel()));
        message.addItem(new GMPField(GMPUtil.DT_EX_SERIAL, ex.getSerial()));
    }

    public static void addProInfo(GMPMessage message, ProcessInfo procInfo){
        message.addItem(new GMPField(GMPUtil.TAG_SEQUNCE,procInfo.getProByte()));
        message.addItem(new GMPField(GMPUtil.TAG_OP_DATE, DataConvert.dateToBCD(procInfo.getDate())));
        message.addItem(new GMPField(GMPUtil.TAG_OP_TIME,DataConvert.timeToBCD(procInfo.getDate())));

    }

    private ProcessInfo getProcessInfo(){
        if(processId>999999){
            processId=0;
        }

        String procesStr = String.format("%06d", ++processId);
        return new ProcessInfo(procesStr);
    }

    public KeyContext getKeyContext() {
        return keyContext;
    }

    private SyncState getNextState() {
        currentState++;
        switch (currentState){
            case 1:return new InitState(keyContext,getProcessInfo());
            case 2:return new KeyReqState(keyContext,getProcessInfo());
            case 3:return new CloseState(keyContext,getProcessInfo());
            default:return null;
        }
    }
}
