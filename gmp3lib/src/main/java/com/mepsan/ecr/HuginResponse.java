package com.mepsan.ecr;

import com.mepsan.gmp.GMPGroup;

import java.util.ArrayList;

/**
 * Created by misman on 16.11.2015.
 */
public class HuginResponse {

    private static final char SPEC_CHAR = '|';
    private String seqNumber;
    private int error;
    private int state;
    private ArrayList<GMPGroup> details;
    private ArrayList<String> rs;

    public HuginResponse() {
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setSeqNumber(String seqNumber) {
        this.seqNumber = seqNumber;
    }

    public void setError(int error) {
        this.error = error;
    }

    public int getErrorNo() {
        return error;
    }

    public String getState() {
        return ECRUtil.GetStateMessage(state);
    }

    public String getError() {
        return ECRUtil.GetErrorMessage(error);
    }

    public void addDetail(GMPGroup detail) {
        if (details==null)
            details = new ArrayList<>();

        this.details.add(detail);
    }

    public GMPGroup getDetailWithTag(int tag){
        for (GMPGroup item : details){
            if(tag == item.getTag())
                return item;
        }
        return null;
    }

    public GMPGroup getDetail() {
        if (details!=null)
            return details.get(0);

        return null;
    }

    public void setRs(ArrayList<String> rs) {
        this.rs = rs;
    }

    public String getRsDetail() {
        String rsStr = String.valueOf(error) + SPEC_CHAR + String.valueOf(state);
        if (rs!=null) {
            for (String det : rs) {
                rsStr += SPEC_CHAR;
                rsStr += det;
            }
        }
        return rsStr;
    }
}
