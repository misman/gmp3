package com.mepsan.ecr;

import com.mepsan.gmp.CRC16;
import com.mepsan.gmp.DataConvert;
import com.mepsan.gmp.GMPUtil;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;

/**
 * Created by misman on 14.10.2015.
 */
public class TCPConnection implements Connection {

    private String ipAddress = "192.168.0.125";
    private int port = 4444;
    private Socket socket;
    private OutputStream out;
    private DataInputStream in;

    public TCPConnection(){}

    public TCPConnection(int port, String ipAddress) {
        this.port = port;
        this.ipAddress = ipAddress;
    }

    @Override
    public void open() {
        try {
            InetSocketAddress serverAddr = new InetSocketAddress(ipAddress,port);
            socket = new Socket();
            socket.connect(serverAddr, 5000);
            socket.setSoTimeout(7000);
            in = new DataInputStream(socket.getInputStream());
            out = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Connect Error");
        }
    }

    @Override
    public boolean sendData(byte[] data){
        if (!isOpen()){
            System.out.println("Not Connected");
            return false;
        }

        int tryCount = 0;

        while (tryCount<3) {

            try {
                out.write(data);
                byte answer = in.readByte();
                if(answer == GMPUtil.ACK){
                    return true;
                }else {
                    System.out.println("Not Accepted");
                    System.out.println(String.format("%02x", answer & 0xff));

                }
                tryCount++;

            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Write or Read Error");
                tryCount++;
            }
        }

        return false;

    }

    @Override
    public byte[] read(){
        try {
            byte start = in.readByte();
            if (start == GMPUtil.STX) {
                int packLength = in.readShort();
                byte[] buffer = new byte[packLength + 2];
                in.readFully(buffer);
                byte end = in.readByte();
                byte[] crc = new byte[2];
                crc[0] = buffer[packLength];
                crc[1] = buffer[packLength + 1];

                buffer = Arrays.copyOf(buffer, packLength);

                String myCRC = DataConvert.byteArrayToHexString(CRC16.CRC16CITT(buffer));
                String comeCRC = DataConvert.byteArrayToHexString(crc);

                if (end==GMPUtil.ETX && myCRC.equals(comeCRC)) {
                    out.write(GMPUtil.ACK);
                    return buffer;
                }else System.out.println("DATA END OR CRC NOT MATCH");
            }
            out.write(GMPUtil.NACK);
            System.out.println("Start Couldn't Find");
            System.out.println(String.format("%02x", start & 0xff));

        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }

        return new byte[0];
    }



    @Override
    public boolean isOpen() {
        return (socket!=null && socket.isConnected());
    }

    @Override
    public void close() {
        try {
            if(isOpen()) {
                out.flush();
                out.close();
                in.close();
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object toObject() {
        return null;
    }

    @Override
    public void setReadTimeOut(int timeOut) {
        try {
            socket.setSoTimeout(timeOut);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
}
